/* Generated by Yosys 0.8+312 (git sha1 22035c20, clang 6.0.0-1ubuntu2 -fPIC -Os) */

(* cells_not_processed =  1  *)
(* src = "my_chip_small.v:1" *)
module my_chip(a_op_pad, b_op_pad, add_pad, sub_pad);
  (* src = "my_chip_small.v:5" *)
  wire a;
  (* src = "my_chip_small.v:2" *)
  input a_op_pad;
  (* src = "my_chip_small.v:5" *)
  wire add;
  (* src = "my_chip_small.v:3" *)
  output add_pad;
  (* src = "my_chip_small.v:5" *)
  wire b;
  (* src = "my_chip_small.v:2" *)
  input b_op_pad;
  (* src = "my_chip_small.v:5" *)
  wire sub;
  (* src = "my_chip_small.v:3" *)
  output sub_pad;
  (* module_not_derived = 32'd1 *)
  (* src = "my_chip_small.v:7" *)
  simple_adder i_simple_adder (
    .a(a_op_pad),
    .b(b_op_pad),
    .c(add)
  );
  (* module_not_derived = 32'd1 *)
  (* src = "my_chip_small.v:8" *)
  simple_sub i_simple_sub (
    .a(a_op_pad),
    .b(b_op_pad),
    .c(sub)
  );
  assign a = a_op_pad;
  assign add_pad = add;
  assign b = b_op_pad;
  assign sub_pad = sub;
endmodule

(* cells_not_processed =  1  *)
(* src = "src/simple_adder.v:1" *)
module simple_adder(a, b, c);
  (* src = "src/simple_adder.v:2" *)
  wire _0_;
  (* src = "src/simple_adder.v:2" *)
  wire _1_;
  (* src = "src/simple_adder.v:3" *)
  wire _2_;
  (* src = "src/simple_adder.v:2" *)
  input a;
  (* src = "src/simple_adder.v:2" *)
  input b;
  (* src = "src/simple_adder.v:3" *)
  output c;
  XOR2X1 _3_ (
    .A(_0_),
    .B(_1_),
    .Y(_2_)
  );
  assign _0_ = a;
  assign _1_ = b;
  assign c = _2_;
endmodule

(* cells_not_processed =  1  *)
(* src = "src/simple_sub.v:1" *)
module simple_sub(a, b, c);
  (* src = "src/simple_sub.v:2" *)
  wire _0_;
  (* src = "src/simple_sub.v:2" *)
  wire _1_;
  (* src = "src/simple_sub.v:3" *)
  wire _2_;
  (* src = "src/simple_sub.v:4|<techmap.v>:258" *)
  wire _3_;
  (* src = "src/simple_sub.v:4|<techmap.v>:260|<techmap.v>:203" *)
  wire _4_;
  (* src = "src/simple_sub.v:2" *)
  input a;
  (* src = "src/simple_sub.v:2" *)
  input b;
  (* src = "src/simple_sub.v:3" *)
  output c;
  XOR2X1 _5_ (
    .A(_1_),
    .B(_0_),
    .Y(_2_)
  );
  assign _1_ = b;
  assign _0_ = a;
  assign c = _2_;
endmodule
