#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from setuptools import setup, find_packages


def readme():
    with open("README.md", "r") as f:
        return f.read()


setup(name='klayout-pnr',
      version='0.0.0',
      description='Simple framework for physical chip design (place & route) based on KLayout.',
      long_description=readme(),
      long_description_content_type="text/markdown",
      keywords='eda framework vlsi place route klayout',
      classifiers=[
          'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
          'Topic :: Scientific/Engineering',
          'Topic :: Scientific/Engineering :: Electronic Design Automation (EDA)',
          'Topic :: Education',
          'Programming Language :: Python :: 3'
      ],
      url='https://codeberg.org/libreda/klayout-pnr',
      author='Thomas Kramer',
      author_email='code@tkramer.ch',
      license='AGPLv3+',
      packages=find_packages(),
      package_data={'': ['test_data/*']},
      include_package_data=True,
      install_requires=[
          'klayout>=0.26.6',
          'numpy==1.*',
          'scipy==1.*',
          'networkx==2.*',
          'verilog-parser',
          'liberty-parser>=0.0.7'
      ],
      zip_safe=False)
