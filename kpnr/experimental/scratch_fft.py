#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Compute electro-static repulsion based on a convolution which is computed via the FFT.
"""

import numpy as np
from typing import Tuple, List
import matplotlib.pyplot as plt
from scipy import interpolate

np.random.seed(1)


class Cell:

    def __init__(self, shape: Tuple[int, int], nets: List[int]):
        self.shape = shape
        self.nets = nets


default_shape = (2, 2)
large_shape = (10, 20)

cells = [
    Cell(default_shape, [0, 1]),
    Cell(default_shape, [0, 2]),
    Cell(large_shape, [1, 2]),
    Cell(default_shape, [2, 3, 4]),
    Cell(default_shape, [0, 3, 4, 7]),
    Cell(default_shape, [7]),
]

cells = [
    Cell(default_shape, [])
]*400

for i in range(0, 10):
    cells.append(Cell(default_shape, []))

num_cells = len(cells)
outline = (100, 100)
x_max, y_max = outline
pos_x = np.random.randint(0, x_max, num_cells)
pos_y = np.random.randint(0, y_max, num_cells)

universe_size = (x_max * 2, y_max * 2)

# Construct kernel for electric potential calculation.
x_space = np.linspace(-x_max / 2, +x_max / 2, x_max)
y_space = np.linspace(-y_max / 2, +y_max / 2, y_max)
xx, yy = np.meshgrid(x_space, y_space)

kernel_canvas = 1 / (xx ** 2 + yy ** 2)

kernel_e = np.zeros(universe_size)
kernel_e[x_max // 2:x_max // 2 + x_max, y_max // 2:y_max // 2 + y_max] = kernel_canvas

# plt.imshow(kernel_e)
# plt.show()

# Shift kernel
kernel_e = np.fft.ifftshift(kernel_e)
# Transform kernel
kernel_e_fft = np.fft.fft2(kernel_e)

# Kernel for derivative
kernel_d = np.zeros(universe_size, dtype=np.complex)
kernel_d_core = np.array([
    [0, -1j, 0],
    [-1, 0, 1],
    [0, 1j, 0]
])
w, h = kernel_d_core.shape
x0 = x_max - w // 2
y0 = y_max - h // 2
kernel_d[x0:x0 + w, y0:y0 + h] = kernel_d_core

kernel_d = np.fft.ifftshift(kernel_d)

kernel_d_fft = np.fft.fft2(kernel_d)

final_kernel = kernel_d_fft * kernel_e_fft


def pixelize_cells(cells: List[Cell], pos_x: np.ndarray, pos_y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    m = np.zeros(universe_size)
    canvas = m[x_max // 2:x_max // 2 + x_max, y_max // 2:y_max // 2 + y_max]

    for cell, x, y in zip(cells, pos_x, pos_y):
        w, h = cell.shape
        x = int(x + 0.5)
        y = int(y + 0.5)
        canvas[x:x + w, y:y + h] += 1
    return m, canvas


def calculate_forces(cells: List[Cell], pos_x: np.ndarray, pos_y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    m, _ = pixelize_cells(cells, pos_x, pos_y)

    m_f = np.fft.fft2(m)
    # plt.imshow(np.abs(canvas))
    # plt.show()

    # plt.imshow(np.abs(final_kernel))
    # plt.show()

    e_field_with_padding = np.fft.ifft2(final_kernel * m_f)

    e_field = e_field_with_padding[x_max // 2:x_max // 2 + x_max, y_max // 2:y_max // 2 + y_max]

    plt.imshow(np.real(e_field))
    plt.show()

    # e_field_interp_imag = interpolate.RectBivariateSpline(x_space, y_space, np.imag(e_field))
    e_field_interp_imag = interpolate.interp2d(x_space, y_space, np.imag(e_field), copy=False)
    # e_field_interp_real = interpolate.RectBivariateSpline(x_space, y_space, np.real(e_field))
    e_field_interp_real = interpolate.interp2d(x_space, y_space, np.real(e_field), copy=False)

    # force_x = e_field_interp_real(pos_x, pos_y)
    # force_y = e_field_interp_imag(pos_x, pos_y)

    force_x = np.array([e_field_interp_real(x, y)[0] for x, y in zip(pos_x, pos_y)])
    force_y = np.array([e_field_interp_imag(x, y)[0] for x, y in zip(pos_x, pos_y)])

    return force_x, force_y


pos_x = pos_x.astype(dtype=np.float)
pos_y = pos_y.astype(dtype=np.float)

cell_width = np.array([c.shape[0] for c in cells])
cell_height = np.array([c.shape[1] for c in cells])

for i in range(0, 10):
    fx, fy = calculate_forces(cells, pos_x + cell_width / 2, pos_y + cell_height / 2)
    # print(fx)
    # print(fy)

    pos_old_x, pos_old_y = pos_x.copy(), pos_y.copy()

    max_force = max(np.max(np.abs(fx)), np.max(np.abs(fy)))
    delta = 10 / max_force
    pos_x += fx * delta
    pos_y += fy * delta

    # Clip to boundaries.
    pos_x = np.maximum(pos_x, 0)
    pos_x = np.minimum(pos_x, outline[0])
    pos_y = np.maximum(pos_y, 0)
    pos_y = np.minimum(pos_y, outline[1])

    if i % 1 == 0:
        _, canvas_old = pixelize_cells(cells, pos_old_x, pos_old_y)
        _, canvas = pixelize_cells(cells, pos_x, pos_y)

        plt.imshow(np.abs(canvas) + 0.2 * np.abs(canvas_old))
        plt.show()
