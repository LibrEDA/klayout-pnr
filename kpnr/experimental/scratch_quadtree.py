#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Compute electro-static repulsion using a quad-tree for approximation.
"""

from kpnr.placer.quadtree import Particle, QuadTree
import matplotlib.pyplot as plt

import numpy as np


def test_quadtree_insert_random():
    from numpy import random
    random.seed(1)

    n = 200
    locations = random.uniform(0.4, 0.6, size=(n, 2))
    charges = np.ones(shape=(n))

    for i in range(0, 101):

        min_x = np.min(locations[:, 0])
        max_x = np.max(locations[:, 0])

        min_y = np.min(locations[:, 1])
        max_y = np.max(locations[:, 1])

        side_len = max(max_x - min_x, max_y - min_y) * 1.001

        tree = QuadTree((min_x, min_y), side_length=side_len)
        particles = [Particle(l, c) for l, c in zip(locations, charges)]
        for p in particles:
            tree.insert(p)

        tree.update_center_of_charge()

        threshold = 0.5
        forces = [tree.force_onto(p, threshold=threshold) for p in particles]

        f_x = np.array([f.x for f in forces])
        f_y = np.array([f.y for f in forces])
        f_abs = np.sqrt(f_x ** 2 + f_y ** 2)
        f_max = np.max(f_abs)

        scale = 0.1 / f_max

        f = np.array([f_x, f_y]).T
        displacement = f * scale

        locations_new = locations + displacement

        locations = locations_new

        l_min = np.zeros_like(locations) + 0
        l_max = np.zeros_like(locations) + 1

        locations = np.maximum(locations, l_min)
        locations = np.minimum(locations, l_max)

        if i % 10 == 0:
            plt.scatter(locations[:, 0], locations[:, 1])
            plt.xlim([0, 1])
            plt.ylim([0, 1])
            plt.show()
