#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import networkx as nx
from . import cell_timing
from .. import test_data
from ..klayout_helper import db
from ..netlist import util as net_util
from typing import Dict, Set, Tuple
import pydot
from enum import Enum

import matplotlib.pyplot as plt

import logging

logger = logging.getLogger(__name__)


class TimingMode(Enum):
    EARLY = 1  # For checking hold violations.
    LATE = 2  # For checking setup violations.


class TimingDirection(Enum):
    AAT = 1  # Actual Arrival Time (forward)
    RAT = 2  # Required Arrival Time (backward)


def compute_load_capacitance(library, subcircuit: db.SubCircuit, output_pin: db.Pin) -> float:
    """
    Compute total load capacitance of a pin by summing up all sink input capacitances.
    :param library:
    :param subcircuit:
    :param output_pin:
    :return: Capacitance value.
    """
    capacitance = 0.0
    # Loop over all cells/pins connected to `output_pin`.
    for neighbour, other_pin in net_util.neighbours(subcircuit, output_pin):
        neighbour_cell_name = neighbour.circuit_ref().name
        neighbour_cell = library.cell_by_name(neighbour_cell_name)
        # Read input capacitance of this pin from the library.
        cap = neighbour_cell.get_input_capacitance(other_pin.name())
        capacitance += cap
    return capacitance


def compute_load_capacitances(library, circuit: db.Circuit) -> Dict[Tuple[int, int], float]:
    """
    Compute load capacitances for all output pins of all cells in the given circuit.
    :param library:
    :param circuit:
    :return: Dictionary mapping (subcircuit.id(), pin.id()) to total load capacitance
    """
    load_capacitances = dict()
    for subcircuit in circuit.each_subcircuit():
        # Compute load capacitance.
        # For each output pin find all connected sink pins and compute the sum of their capacitance.
        for pin in net_util.get_output_pins(subcircuit):
            load_capacitances[(subcircuit.id(), pin.id())] = compute_load_capacitance(library, subcircuit, pin)
    return load_capacitances


def generate_signal_flow_graph(library, circuit: db.Circuit) -> nx.DiGraph:
    """
    Convert all cells in the circuit in to nodes. Insert an edge for all connected cells.
    :param library:
    :param circuit:
    :return:
    """
    pass


def compute_slews(library, circuit: db.Circuit) -> Dict[Tuple[int, int], Tuple[float, float]]:
    pass


class Clock:

    def __init__(self, name: str, period: float):
        self.name = name
        self.period = period


def create_timing_graph(library, top: db.Circuit,
                        timing_mode: TimingMode,
                        timing_direction: TimingDirection,
                        clocks: Set[Clock]) -> nx.DiGraph:
    assert isinstance(top, db.Circuit)
    assert isinstance(timing_mode, TimingMode)
    assert isinstance(timing_direction, TimingDirection)

    graph = nx.DiGraph()

    def node_name(subcircuit: db.SubCircuit, pin: db.Pin) -> str:
        """
        Construct a graph node name.
        :param subcircuit:
        :param pin:
        :return:
        """
        return "{} {} {}".format(subcircuit.circuit_ref().name, subcircuit.name, pin.name())

    # Handle ports.
    for p in top.each_pin():
        graph.add_node(p.name())
        net = top.net_for_pin(p)

        if net_util.is_input_pin(p):
            graph.add_edge('source', p.name(), weight=0)

        if net_util.is_output_pin(p):
            graph.add_edge(p.name(), 'sink', weight=0)

        for pin_ref in net.each_subcircuit_pin():
            pin = pin_ref.pin()
            c = pin_ref.subcircuit()
            if net_util.is_input_pin(pin):
                graph.add_edge(p.name(), node_name(c, pin), weight=0)
            else:
                graph.add_edge(node_name(c, pin), p.name(), weight=0)

    # Compute total load capacitances.
    load_capacitances = compute_load_capacitances(library, top)

    # Handle internal connections.
    for subcircuit in top.each_subcircuit():
        subcircuit_ref = subcircuit.circuit_ref()
        cell_name = subcircuit_ref.name
        cell = library.cell_by_name(cell_name)

        # Create cell internal edges.
        for timing_arc in cell.get_all_timing_arcs():
            related_pin_name = timing_arc.related_pin
            pin_name = timing_arc.pin
            print(cell_name, related_pin_name, '->', pin_name)
            related_pin = subcircuit_ref.pin_by_name(related_pin_name)
            pin = subcircuit_ref.pin_by_name(pin_name)

            print(' ', timing_arc.get_timing_types())
            if 'combinational' in timing_arc.timings:
                # Delay.
                _output_load = load_capacitances[(subcircuit.id(), pin.id())]

                cell_rise = timing_arc.get_delay('combinational', 'cell_rise',
                                                 total_output_net_capacitance=_output_load,
                                                 input_net_transition=0)

                cell_fall = timing_arc.get_delay('combinational', 'cell_fall',
                                                 total_output_net_capacitance=_output_load,
                                                 input_net_transition=0)

                # Find delay appropriate for the selected timing mode.
                if timing_mode == TimingMode.EARLY:
                    delay = min(cell_fall, cell_rise)
                else:
                    assert timing_mode == TimingMode.LATE
                    delay = max(cell_fall, cell_rise)

                graph.add_edge(node_name(subcircuit, related_pin),
                               node_name(subcircuit, pin),
                               weight=delay,
                               type='delay')

            if 'rising_edge' in timing_arc.timings:
                # Delay from clock to data output (flip-flops).

                _output_load = load_capacitances[(subcircuit.id(), pin.id())]
                cell_rise = timing_arc.get_delay('rising_edge', 'cell_rise',
                                                 total_output_net_capacitance=_output_load,
                                                 input_net_transition=0)

                cell_fall = timing_arc.get_delay('rising_edge', 'cell_fall',
                                                 total_output_net_capacitance=_output_load,
                                                 input_net_transition=0)

                # Find delay appropriate for the selected timing mode.
                if timing_mode == TimingMode.EARLY:
                    delay = min(cell_fall, cell_rise)
                elif timing_mode == TimingMode.LATE:
                    delay = max(cell_fall, cell_rise)

                graph.add_edge(node_name(subcircuit, related_pin),
                               node_name(subcircuit, pin),
                               weight=delay,
                               type='delay')

            if 'falling_edge' in timing_arc.timings:
                # Delay from clock to data output (flip-flops).
                _output_load = load_capacitances[(subcircuit.id(), pin.id())]
                cell_rise = timing_arc.get_delay('falling_edge', 'cell_rise',
                                                 total_output_net_capacitance=_output_load,
                                                 input_net_transition=0)

                cell_fall = timing_arc.get_delay('falling_edge', 'cell_fall',
                                                 total_output_net_capacitance=_output_load,
                                                 input_net_transition=0)

                # Find delay appropriate for the selected timing mode.
                if timing_mode == TimingMode.EARLY:
                    delay = min(cell_fall, cell_rise)
                elif timing_mode == TimingMode.LATE:
                    delay = max(cell_fall, cell_rise)

                graph.add_edge(node_name(subcircuit, related_pin),
                               node_name(subcircuit, pin),
                               weight=delay,
                               type='delay')

            if timing_mode == TimingMode.EARLY:
                if timing_direction == TimingDirection.RAT:
                    # Insert edges with hold constraint.

                    if 'hold_rising' in timing_arc.timings:
                        # TODO: find transition times.
                        logger.warning('Using t=0 for related_pin_transition and constrained_pin_transition')

                        related_pin_transition = 0.6  # Dummy values.
                        constrained_pin_transition = 1.8

                        rise_constraint = timing_arc.get_constraint('hold_rising', 'rise_constraint',
                                                                    related_pin_transition=related_pin_transition,
                                                                    constrained_pin_transition=constrained_pin_transition)

                        fall_constraint = timing_arc.get_constraint('hold_rising', 'fall_constraint',
                                                                    related_pin_transition=related_pin_transition,
                                                                    constrained_pin_transition=constrained_pin_transition)

                        # Be pessimistic.
                        # Signal is required to arrive at earliest `constraint` after clock.
                        constraint = min(rise_constraint, fall_constraint)

                        graph.add_edge(node_name(subcircuit, related_pin),
                                       node_name(subcircuit, pin),
                                       weight=constraint,
                                       type='constraint')

            if timing_mode == TimingMode.LATE:
                if timing_direction == TimingDirection.RAT:
                    if 'setup_rising' in timing_arc.timings:
                        # TODO: find transition times.
                        logger.warning('Using t=0 for related_pin_transition and constrained_pin_transition')

                        related_pin_transition = 0.6  # Dummy values.
                        constrained_pin_transition = 1.8

                        rise_constraint = timing_arc.get_constraint('setup_rising', 'rise_constraint',
                                                                    related_pin_transition=related_pin_transition,
                                                                    constrained_pin_transition=constrained_pin_transition)

                        fall_constraint = timing_arc.get_constraint('setup_rising', 'rise_constraint',
                                                                    related_pin_transition=related_pin_transition,
                                                                    constrained_pin_transition=constrained_pin_transition)
                        # Be pessimistic.
                        # Signal is required to arrive at latest `constraint` before clock.
                        constraint = - max(rise_constraint, fall_constraint)

                        graph.add_edge(node_name(subcircuit, related_pin),
                                       node_name(subcircuit, pin),
                                       weight=constraint,
                                       type='constraint')

        # Find connections to other subcircuits.
        for pin in net_util.get_output_pins(subcircuit):
            downstream = net_util.neighbours(subcircuit, pin)

            for c, p in downstream:
                graph.add_edge(node_name(subcircuit, pin), node_name(c, p),
                               weight=0)

    # Check that there are no cycles.
    assert len(list(nx.simple_cycles(graph))) == 0

    return graph


def test_build_timing_graph():
    # Construct timing graph from a netlist and plot it.

    # Read netlist and library.
    netlist: db.Netlist = test_data.get_example_netlist('seq_chip_simple_nl.v')
    net_util.flatten(netlist)

    liberty = test_data.get_example_liberty()

    library = cell_timing.create_cell_library(liberty)

    top = netlist.circuit_by_name('seq_chip')
    assert top is not None

    timing_mode = TimingMode.EARLY

    # Define clock signals.
    clocks = {Clock('CLK', 10)}

    graph = create_timing_graph(library, top,
                                timing_mode=timing_mode,
                                timing_direction=TimingDirection.AAT,
                                clocks=clocks)

    # Plot the graph using Graphviz.
    dot: pydot.Graph = nx.nx_pydot.to_pydot(graph)

    # Find node positions using Graphviz.
    pos = nx.nx_pydot.pydot_layout(graph, prog='dot')

    # graph = graph.reverse(copy=True)
    def negated_weight(a, b, data) -> float:
        return -data['weight']

    aat = nx.single_source_dijkstra_path_length(graph, 'source', weight='weight')
    print(aat)

    graph_rat = create_timing_graph(library, top,
                                    timing_mode=timing_mode,
                                    timing_direction=TimingDirection.AAT,
                                    clocks=clocks)
    graph_rat = graph_rat.reverse()
    rat = nx.single_source_dijkstra_path_length(graph_rat, 'sink', weight=negated_weight)

    slack = {node: rat[node] - aat[node] for node in aat.keys() if node in rat and node in aat}
    print(slack)

    nx.draw_networkx(graph, pos, with_labels=False)
    nx.draw_networkx_labels(graph, pos, {n: "{}, AT={}, RT={}".format(n, t, rat.get(n, '?')) for n, t in aat.items()})
    nx.draw_networkx_edge_labels(graph, pos)
    plt.show()

    # tmp = tempfile.mktemp(suffix='.dot')
    # dot.write(tmp)
    # subprocess.run(['xdot', tmp])
    # os.remove(tmp)
