#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import liberty.types
import liberty.parser
import numpy as np
from scipy import interpolate
from ..test_data import *
import networkx as nx
from networkx import multi_source_dijkstra_path_length
from typing import Any, Dict, Tuple, Set, List

import logging

logger = logging.getLogger(__name__)


class TimingArc:

    def __init__(self, pin: str, related_pin: str):
        self.pin: str = pin
        self.related_pin: str = related_pin

        # Mapping from timing type x timing name -> (variable names, interpolated timing as function of load and transition time).
        self.timings: Dict[str, Dict[str, Tuple[Tuple[str], Any]]] = dict()

        # Mapping from 'power type' -> (variable names, interpolated power function).
        self.power: Dict[str, Tuple[Tuple[str], Any]] = dict()

    def get_timing_types(self) -> Set[str]:
        """
        Get a set of all timing types which are used as key in `get_timing_function`.
        :return:
        """
        return set(self.timings.keys())

    def get_timing_function(self,
                            timing_type: str,
                            timing_name: str):
        timing_by_name = self.timings.get(timing_type)
        assert timing_by_name is not None, 'No such timing type: {}. Available: {}'.format(timing_type,
                                                                                           list(self.timings.keys()))

        variable_names, timing = timing_by_name.get(timing_name)

        assert timing is not None, 'No such timing: {}. Available: {}'.format(timing_name, list(timing_by_name.keys()))
        return timing

    def get_power_function(self,
                           power_type: str):
        power_by_name = self.power.get(power_type)
        assert power_by_name is not None, 'No such power type: {}. Available: {}'.format(power_type,
                                                                                         list(self.power.keys()))

        variable_names, power_function = power_by_name

        return power_function

    def get_energy(self,
                   power_type: str,
                   total_output_net_capacitance: float,
                   input_net_transition: float
                   ):
        """
        Get interpolated power value at (total_output_net_capacitance, input_net_transition).
        :param power_type:
        :param total_output_net_capacitance:
        :param input_transition_time:
        :return: Energy value.
        """

        return self.get_power_function(power_type)(
            total_output_net_capacitance=total_output_net_capacitance,
            input_transition_time=input_net_transition
            # TODO: Why is this called `input_transition_time` and not `input_transition_time`?
        )[0]

    def get_delay(self,
                  timing_type: str,
                  timing_name: str,
                  total_output_net_capacitance: float,
                  input_net_transition: float
                  ) -> float:
        """
        Get interpolated delay value at (total_output_net_capacitance, input_net_transition).

        :param timing_type:
        :param timing_name:
        :param total_output_net_capacitance:
        :param input_net_transition:
        :return:
        """

        return self.get_timing_function(
            timing_type,
            timing_name)(
            total_output_net_capacitance=total_output_net_capacitance,
            input_net_transition=input_net_transition
        )[0]

    def get_constraint(self,
                       timing_type: str,
                       timing_name: str,
                       related_pin_transition: float,
                       constrained_pin_transition: float
                       ) -> float:
        """
        Get interpolated constraint function and evaluate it at (output_load, input_slew).

        :param timing_type:
        :param timing_name:
        :param output_load:
        :param input_slew:
        :return:
        """

        return self.get_timing_function(
            timing_type,
            timing_name)(related_pin_transition=related_pin_transition,
                         constrained_pin_transition=constrained_pin_transition)[0]


class Cell:
    """
    This class is used as a container for look-up tables constructed based on a liberty library.
    """

    def __init__(self, name: str):
        self.name: str = name
        self.input_pins: Set[str] = set()
        self.output_pins: Set[str] = set()
        self.clock_input_pins: Set[str] = set()
        self.capacitance: Dict[str, float] = dict()
        self.rise_capacitance: Dict[str, float] = dict()
        self.fall_capacitance: Dict[str, float] = dict()

        # Mapping from (input pin, output pin) x timing type x timing name -> interpolated timing as function of load and transition time.
        self.timing_arcs: Dict[Tuple[str, str], TimingArc] = dict()

        # # Key: (input pin, output pin)
        # self.cell_rise_default: Dict[Tuple[str, str], Any] = dict()
        # self.cell_fall_default: Dict[Tuple[str, str], Any] = dict()
        #
        # self.rise_transition: Dict[Tuple[str, str], Any] = dict()
        # self.fall_transition: Dict[Tuple[str, str], Any] = dict()

    def get_input_capacitance(self, pin_name: str) -> float:
        return float(self.capacitance[pin_name])

    def get_all_timing_arcs(self) -> Set[TimingArc]:
        return set(self.timing_arcs.values())

    def get_timing_arc(self, input_pin: str,
                       output_pin: str) -> TimingArc:
        timing_arc = self.timing_arcs.get((input_pin, output_pin))

        assert timing_arc is not None, \
            'No such timing arc: {} -> {}. Available arcs: {}'.format(input_pin,
                                                                      output_pin,
                                                                      list(self.timing_arcs.keys()))
        return timing_arc

    # def get_rise_delay_function(self,
    #                             input_pin: str,
    #                             output_pin: str) -> Any:
    #     return self.cell_rise_default[(input_pin, output_pin)]
    #
    # def get_rise_delay(self,
    #                    input_pin: str,
    #                    output_pin: str,
    #                    output_load: float,
    #                    input_slew: float) -> float:
    #     return self.get_rise_delay_function(input_pin, output_pin)(output_load, input_slew)[0]
    #
    # def get_fall_delay_function(self,
    #                             input_pin: str,
    #                             output_pin: str) -> Any:
    #     return self.cell_fall_default[(input_pin, output_pin)]
    #
    # def get_fall_delay(self,
    #                    input_pin: str,
    #                    output_pin: str,
    #                    output_load: float,
    #                    input_slew: float) -> float:
    #     return self.get_fall_delay_function(input_pin, output_pin)(output_load, input_slew)[0]
    #
    # def get_rise_transition_function(self,
    #                                  input_pin: str,
    #                                  output_pin: str) -> Any:
    #     return self.rise_transition[(input_pin, output_pin)]
    #
    # def get_rise_transition(self,
    #                         input_pin: str,
    #                         output_pin: str,
    #                         output_load: float,
    #                         input_slew: float) -> float:
    #     return self.get_rise_transition_function(input_pin, output_pin)(output_load, input_slew)[0]
    #
    # def get_fall_transition_function(self,
    #                                  input_pin: str,
    #                                  output_pin: str) -> Any:
    #     return self.fall_transition[(input_pin, output_pin)]
    #
    # def get_fall_transition(self,
    #                         input_pin: str,
    #                         output_pin: str,
    #                         output_load: float,
    #                         input_slew: float) -> float:
    #     return self.get_fall_transition_function(input_pin, output_pin)(output_load, input_slew)[0]


class CellLibrary:

    def __init__(self):
        self.cells: Dict[str, Cell] = dict()

    def add_cell(self, cell: Cell):
        assert cell.name not in self, "Cell '{}' already in library.".format(cell.name)
        self.cells[cell.name] = cell

    def cell_by_name(self, name: str):
        return self.cells[name]

    def __contains__(self, item):
        return item in self.cells


def _get_interpolation(delay_group: liberty.types.Group) -> Any:
    is_scalar = delay_group.args[0] == 'scalar'

    is_2d = 'index_2' in delay_group

    if is_scalar:
        values = delay_group.get_array('values')
        value = values[0]
        return lambda x: value
    elif not is_2d:
        assert 'index_1' in delay_group
        index = delay_group.get_array('index_1')[0]
        values = delay_group.get_array('values')[0]
        interpolated = interpolate.interp1d(index, values)
        return interpolated
    else:
        assert 'index_1' in delay_group
        index1 = delay_group.get_array('index_1')
        index2 = delay_group.get_array('index_2')
        cell_rise_values = delay_group.get_array('values').T
        interpolated = interpolate.interp2d(index1, index2, cell_rise_values)
        return interpolated


def _get_lut_variable_names(library: liberty.types.Group, lut_type: str, lut_name: str) -> List[str]:
    """
    Find the names of the LUT indices.
    :param library: Liberty library.
    :param lut_type: The group name of the LUT table template, e.g. `lu_table_template`.
    :param lut_name: Name of the LUT template.
    :return: List of index names.
    """
    if lut_name != 'scalar':
        # Get lookup table template.
        template = library.get_group(lut_type, lut_name)

        # Find out which axis of the LUT corresponds to which variable.
        variable_1_name = template.get('variable_1')
        variable_2_name = template.get('variable_2')

        if variable_2_name is not None:
            variable_names = [variable_1_name, variable_2_name]
        else:
            variable_names = [variable_1_name]
    else:
        variable_names = ['scalar']

    return variable_names


def create_lut_function(liberty_library, group, template_type: str) -> Tuple[List[str], Any]:
    """
    Read lookup table into a interpolated lambda function.

    :param liberty_library: Full liberty library. This is needed to find the look-up table templates.
    :param group: Group containing the lookup table. Must contain attributes `index_*` and `values`.
    :param template_type: Name of the lookup table template group,  e.g. 'lu_table_template'.
    :return: (list of function parameter names, function)
    """
    # Get the look-up table template and check which axis corresponds to which variable.
    table_template_name = group.args[0]
    variable_names = _get_lut_variable_names(liberty_library,
                                             template_type,
                                             table_template_name)

    # Create 'closure'. Results in a function that evaluates the interpolated lookup table with
    # named indices.
    def create_closure(variable_names, interpolated):
        def _function(**args):
            _args = [args[name] for name in variable_names]
            return interpolated(*_args)

        return _function

    interpolated_lut_function = create_closure(variable_names, _get_interpolation(group))

    return variable_names, interpolated_lut_function


def create_cell_library(liberty_library: liberty.types.Group) -> CellLibrary:
    """
    Generate efficient lookup tables from the liberty data structure.
    :param liberty_library:
    :return:
    """
    lib = CellLibrary()

    for cell_group in liberty_library.get_groups('cell'):
        cell_name = cell_group.args[0]

        cell = Cell(cell_name)
        lib.add_cell(cell)

        is_flipflop = len(cell_group.get_groups('ff')) == 1
        is_latch = len(cell_group.get_groups('latch')) == 1

        is_combinational = not is_flipflop and not is_latch

        for pin_group in cell_group.get_groups('pin'):
            pin_name = pin_group.args[0]

            # Store pin direction.
            direction = pin_group['direction']
            if direction == 'input':
                cell.input_pins.add(direction)
            else:
                cell.output_pins.add(direction)

            cell.capacitance[pin_name] = float(pin_group['capacitance'])
            cell.rise_capacitance[pin_name] = float(pin_group['rise_capacitance'])
            cell.fall_capacitance[pin_name] = float(pin_group['fall_capacitance'])

            is_clock = pin_group.get('clock', False)
            if is_clock:
                cell.clock_input_pins.add(pin_name)

            for timing_group in pin_group.get_groups('timing'):

                related_pin = timing_group['related_pin'].value
                timing_arc = (related_pin, pin_name)
                timing_type = timing_group.get('timing_type')

                if timing_type is None:
                    logger.warning(
                        '`timing_type` not defined for {} {}->{}. Use `combinational`'.format(cell_name, related_pin,
                                                                                              pin_name))
                    timing_type = 'combinational'

                # if timing_type == 'hold_rising':
                #     # Hold time for positive-edge triggered flip-flops.
                #     pass
                # elif timing_type == 'hold_falling':
                #     # Hold time for negative-edge triggered flip-flops.
                #     pass
                # elif timing_type == 'setup_rising':
                #     # Setup time for positive-edge triggered flip-flops.
                #     pass
                # elif timing_type == 'setup_falling':
                #     # Setup time for negative-edge triggered flip-flops.
                #     pass
                # elif timing_type == 'rising_edge':
                #     # Used for clock->output arc in positive-edge triggered flip-flops
                #     # and latches (transparent on high clock).
                #     pass
                # elif timing_type == 'falling_edge':
                #     # Used for clock->output arc in positive-edge triggered flip-flops
                #     # and latches (transparent on low clock).
                #     pass
                # elif timing_type == 'preset':
                #     # Set->output arc of flip-flops.
                #     pass
                # elif timing_type == 'clear':
                #     # Reset->output arc of flip-flops.
                #     pass
                # elif timing_type == 'recovery_falling':
                #     pass
                # elif timing_type == 'recovery_rising':
                #     pass
                # elif timing_type == 'removal_falling':
                #     pass
                # elif timing_type == 'removal_rising':
                #     pass
                # elif timing_type == 'three_state_enable':
                #     pass
                # elif timing_type == 'three_state_disable':
                #     pass
                # elif timing_type is None:
                #     pass

                # Read all lookup tables for this timing arc and store them.
                timing_arc = cell.timing_arcs.setdefault(timing_arc, TimingArc(pin_name, related_pin))
                timings = timing_arc.timings.setdefault(timing_type, dict())
                for timing in timing_group.groups:
                    timing_name = timing.group_name

                    variable_names, interpolated_lut_function = create_lut_function(liberty_library,
                                                                                    timing,
                                                                                    'lu_table_template')

                    # Store interpolation function of the lookup-table together with the variable names.
                    timings[timing_name] = (variable_names, interpolated_lut_function)

            # Read power LUTs.
            for power_group in pin_group.get_groups('internal_power'):

                # Input pins into sequential cells such as D or CLK might not have a related pin.
                related_pin = power_group.get('related_pin', None)
                if related_pin:
                    related_pin = related_pin.value

                timing_arc = (related_pin, pin_name)

                timing_arc = cell.timing_arcs.setdefault(timing_arc, TimingArc(pin_name, related_pin))

                for power in power_group.groups:
                    power_name = power.group_name

                    variable_names, interpolated_lut_function = create_lut_function(liberty_library,
                                                                                    power,
                                                                                    'power_lut_template')

                    # Store interpolation function of the lookup-table together with the variable names.
                    timing_arc.power[power_name] = (variable_names, interpolated_lut_function)

    return lib


def test_get_cell_delay():
    library = liberty.parser.parse_liberty(get_example_liberty_library())

    # delay = get_cell_delay_function(library,
    #                                 cell_name='INVX1',
    #                                 input_slew=0,
    #                                 load_capacitance=2)
    # print('delay=', delay)

    ct = create_cell_library(library)
    c = ct.cell_by_name('INVX1')
    print('Capacitance =', c.capacitance)
    arc = c.get_timing_arc('A', 'Y')
    print('Timing arc A -> Y')
    print('Delay =', arc.get_delay('combinational', 'cell_rise',
                                   total_output_net_capacitance=0,
                                   input_net_transition=0))

    print('Energy =', arc.get_energy('rise_power',
                                     total_output_net_capacitance=0,
                                     input_net_transition=0))
