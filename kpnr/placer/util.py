#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from scipy.sparse import lil_matrix, csc_matrix, spmatrix
from scipy.sparse.linalg import spsolve, cg

import numpy as np
from typing import List, Dict, Tuple

import networkx as nx

import logging

logger = logging.getLogger(__name__)


def _get_free_node_indices(graph: nx.Graph) -> np.ndarray:
    """Get names of nodes whose position is not fixed."""
    nodelist = [n for n, data in graph.nodes(data=True) if not data.get('fixed', False)]
    nodelist = np.array(nodelist)
    nodelist.sort()
    return nodelist


def _get_fixed_node_indices(graph: nx.Graph) -> np.ndarray:
    """Get names of nodes whose position is fixed."""
    nodelist = [n for n, data in graph.nodes(data=True) if data.get('fixed', False)]
    nodelist = np.array(nodelist)
    nodelist.sort()
    return nodelist


def _generate_test_graph(num_nodes=200, num_random_edges=40):
    """
    Generate a random graph for testing purpose.
    :return:
    """
    g = nx.MultiGraph()
    g.add_node(0, position=(0, 0), fixed=True)
    g.add_node(1, position=(10, 10), fixed=True)
    g.add_node(2, position=(10, 0), fixed=True)
    g.add_node(3, position=(0, 10), fixed=True)

    g.add_node(4, position=(0, 5), fixed=True)
    g.add_node(5, position=(5, 0), fixed=True)
    g.add_node(6, position=(10, 5), fixed=True)
    g.add_node(7, position=(5, 10), fixed=True)

    # Connect cells serially
    r = np.arange(num_nodes)
    edges = np.array([r, (r + 1) % num_nodes]).T
    for u, v in edges:
        g.add_edge(u, v, weight=1)

    # Add some random edges
    np.random.seed(1)
    edges = np.random.randint(0, num_nodes, (num_random_edges, 2))
    for u, v in edges:
        g.add_edge(u, v, weight=1)

    assert nx.is_connected(g)

    return g
