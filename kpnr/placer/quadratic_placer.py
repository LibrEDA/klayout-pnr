#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from scipy import sparse
from scipy.sparse import linalg

import math
import numpy as np
from typing import Tuple

import networkx as nx

import logging
from ..placer import util
from .placer import StdCellPlacer

logger = logging.getLogger(__name__)


class QuadraticPlacer(StdCellPlacer):

    def __init__(self):
        super().__init__()
        self.enable_convert_nodes_to_integers(True)

    def place_impl(self, graph: nx.MultiGraph, core_area) -> Tuple[np.ndarray, np.ndarray]:
        """
        :param graph:
        :param core_area: Is ignored.
        :return:
        """
        return place_quadratic(graph)


"""
Solve A*x = b_x, A*y = b_y.

Construct A:
Get connectivity matrix C first. A = -C + d
d is a diagonal matrix where d[i, i] = sum(row(C, i)) + (sum of weights of all wires from cell i to pads)

Construct b_x:
If gate i connects to a pad at (x_i, y_i) with a wire with weight w_i: b_x[i] = w_i * x_i
Same for b_y.
"""


def _build_connectivity_matrix(graph: nx.Graph, free_node_indices: np.ndarray) -> sparse.spmatrix:
    """
    Build a sparse connectivity matrix.
    :param graph: Graph with nodes representing cell pins and edges representing wires.
    :param free_node_indices: Names of nodes to be respected for building b. This is produced by _get_free_node_indices.
    :return: (Connectivity matrix in a sparse format, corresponding node indices).
    """
    assert isinstance(graph, nx.MultiGraph) or isinstance(graph, nx.Graph)

    # Fix: nx.to_scipy_sparse_matrix fails to handle case of empty graph.
    if len(free_node_indices) == 0:
        return sparse.csr_matrix(np.zeros((0, 0)))

    # Create sparse connectivity matrix.
    connectivity_matrix = nx.to_scipy_sparse_matrix(graph,
                                                    nodelist=free_node_indices,
                                                    weight='weight',
                                                    dtype=np.float)

    return connectivity_matrix


def _build_b(graph: nx.MultiGraph, free_node_indices: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    Construct b_x and b_y that will be used for solving A*x = b_x, A*y = b_y.

    Construct b_x:
    If gate i connects to a pad at (x_i, y_i) with a wire with weight w_i, then b_x[i] = w_i * x_i else b_x[i] = 0
    Same for b_y.
    :param graph:
    :param free_node_indices: Names of nodes to be respected for building b. This is produced by _get_free_node_indices.
    :return: (b_x, b_y).
    """
    assert isinstance(graph, nx.MultiGraph)
    logger.debug("Building b_x and b_y.")

    free_nodelist = free_node_indices
    num_free_cells = len(free_nodelist)

    bx = np.zeros(num_free_cells, dtype=np.float)
    by = np.zeros(num_free_cells, dtype=np.float)

    # Loop over all free cells and check if they are connected to a fixed pin.
    isfixed = nx.get_node_attributes(graph, 'fixed')
    positions = nx.get_node_attributes(graph, 'position')
    for i, n in enumerate(free_nodelist):
        x = 0
        y = 0
        for neighbour in graph[n]:
            if isfixed.get(neighbour, False):
                assert neighbour in positions, "Fixed pin has no position defined."
                _x, _y = positions[neighbour]

                edges_data = graph[n][neighbour]
                for edge_data in edges_data.values():
                    w = edge_data['weight']
                    assert w >= 0, "negative weights not supported."
                    x += _x * w
                    y += _y * w

        # Write to b vectors
        bx[i] = x
        by[i] = y

    return bx, by


def _build_matrix_a(graph: nx.Graph, free_node_indices: np.ndarray) -> sparse.spmatrix:
    """
    Generate matrix A that can be used to solve for the positions by solving A*x = b_x.

    A is constructed as follows:
    Get connectivity matrix C first. A = -C + d
    d is a diagonal matrix where d[i, i] = sum(row(C, i)) + (sum of weights of all wires from cell i to pads)

    :param graph: Graph with nodes representing cell pins and edges representing wires.
    :param free_node_indices: Names of nodes to be respected for building b. This is produced by _get_free_node_indices.
    :return: (Matrix A in sparse format, corresponding node indices).
    """
    assert isinstance(graph, nx.MultiGraph)
    logger.debug("Building matrix A.")

    C = _build_connectivity_matrix(graph, free_node_indices)
    A = sparse.lil_matrix(-C, dtype=np.float)
    # Get sum of rows.
    row_sum = np.sum(C, axis=1).T

    num_free_cells, _ = A.shape
    assert A.shape == (num_free_cells, num_free_cells)

    pad_weights = np.zeros(num_free_cells, dtype=np.float)

    # Sum up weights of edges to fixed nodes.
    isfixed = nx.get_node_attributes(graph, 'fixed')
    for i, n in enumerate(free_node_indices):
        w = 0
        for neighbour in graph[n]:
            if isfixed.get(neighbour, False):
                edges_data = graph[n][neighbour]
                for edge_data in edges_data.values():
                    _w = edge_data['weight']
                    assert _w >= 0, "negative weights not supported."
                    w += _w
        pad_weights[i] += w

    diag = row_sum + pad_weights
    for i, val in enumerate(diag.flat):
        A[i, i] = val

    return sparse.csc_matrix(A)


def test_build_graph():
    g = nx.MultiGraph()
    g.add_node(1, position=(0, 0))
    g.add_node(2, position=(1, 1))
    g.add_node(3, position=(1, 0))
    g.add_node(4, position=(1, 0), fixed=True)
    g.add_edge(1, 2, 'a', weight=0.5)
    g.add_edge(3, 2, 'b', weight=0.2)
    g.add_edge(3, 2, 'c', weight=0.1)

    free_nodelist = [n for n, data in g.nodes(data=True) if not data.get('fixed', False)]
    free_nodelist.sort()

    adj_sparse = nx.to_scipy_sparse_matrix(g, nodelist=free_nodelist)


def test_build_connectivity_matrix():
    g = nx.MultiGraph()
    g.add_edges_from([
        (0, 1),
        (0, 2),
        (1, 2),
    ])
    idx = util._get_free_node_indices(g)
    C = _build_connectivity_matrix(g, idx)

    assert ((C.toarray() != 0) == np.array([[0, 1, 1], [1, 0, 1], [1, 1, 0]])).all()


def test_build_a():
    g = nx.MultiGraph()
    g.add_node(0, position=(3, 7), fixed=True)
    g.add_node(1, position=(13, 17), fixed=True)
    data = {
        'weight': 1
    }
    g.add_edges_from([
        (2, 0, data),
        (2, 3, data),
        (3, 4, data),
        (4, 1, data)
    ])
    free_node_indices = util._get_free_node_indices(g)
    A = _build_matrix_a(g, free_node_indices)

    bx, by = _build_b(g, free_node_indices)

    x = linalg.bicgstab(A, bx)


def place_quadratic(graph: nx.MultiGraph) -> Tuple[np.ndarray, np.ndarray]:
    """
    Find a placement of the graph nodes which minimizes the quadratic wirelength.
    Wires are defined by graph edges and weighted by the 'weight' attribute of the edges.
    Nodes may have the 'position' attribute which defines an initial position as (x,y) coordinates,
    the 'fixed' attribute tells if a nodes position is fixed and will not be changed during optimization.
    This should be used for pins pre-placed macros such as pads.
    :param graph: Graph representing the cells to be placed and the wires.
    :return: Returns a tuple of (solution, node_indices) where solution is a 2*n numpy array holding x,y coordinates of the
        placement and node_indices is a 1*n numpy array which holds the names/indices of the corresponding graph nodes.
    """

    assert isinstance(graph, nx.MultiGraph)

    free_node_indices = util._get_free_node_indices(graph)
    fixed_node_indices = util._get_fixed_node_indices(graph)
    A = _build_matrix_a(graph, free_node_indices)
    bx, by = _build_b(graph, free_node_indices)

    # TODO: provide starting guess to `cg` based on provided node positions.

    # Solve x coordinates.
    logger.info("Solving quadratic placement problem A*x=b_x.")
    x, _ = linalg.cg(A, bx)
    # Solve y coordinates.
    logger.info("Solving quadratic placement problem A*y=b_y.")
    y, _ = linalg.cg(A, by)

    # Get coordinates of fixed nodes.
    positions = nx.get_node_attributes(graph, 'position')
    fixed_coords = np.array([positions[i] for i in fixed_node_indices])
    x_fixed = fixed_coords[:, 0]
    y_fixed = fixed_coords[:, 1]

    # Append fixed coordinates
    x = np.concatenate([x_fixed, x])
    y = np.concatenate([y_fixed, y])
    node_indices = np.concatenate([fixed_node_indices, free_node_indices])

    solution = np.array([x, y]).T
    return solution, node_indices


def test_place_quadratic():
    import matplotlib.pyplot as plt
    g = util._generate_test_graph()

    solution, idx = place_quadratic(g)

    pos = {i: (x, y) for i, (x, y) in zip(idx, solution)}

    nx.draw_networkx(g, pos, with_labels=True)
    plt.show()


def test_place_quadratic_edge_weights():
    # Test that edge weights are included in A and b.

    g = nx.MultiGraph()
    g.add_node(0, position=(0, 0), fixed=True)
    g.add_node(1, position=(10, 100), fixed=True)
    g.add_node(2)  # Flexible node.

    g.add_edge(0, 2, weight=1)
    g.add_edge(1, 2, weight=3)

    solution, idx = place_quadratic(g)

    pos = {i: (x, y) for i, (x, y) in zip(idx, solution)}

    # x2 must minimize f(x) = x**2 + (10-x)**2 * 3
    # => x2 == 7.5
    # Same for y but multiplied with factor 10.

    x2, y2 = pos[2]
    assert math.isclose(x2, 7.5)
    assert math.isclose(y2, 75)


def test_place_quadratic_edge_weights2():
    # Test that edge weights are included in A and b.

    g = nx.MultiGraph()
    g.add_node(1, position=(0, 0), fixed=True)
    g.add_node(2, position=(10, 100), fixed=True)
    g.add_node(3, position=(10, 100), fixed=True)
    g.add_node(0)  # Flexible node.

    g.add_edge(0, 1, weight=1)
    g.add_edge(0, 2, weight=1)
    g.add_edge(0, 3, weight=2)

    solution, idx = place_quadratic(g)

    pos = {i: (x, y) for i, (x, y) in zip(idx, solution)}

    # x0 must minimize f(x) = x**2 + (10-x)**2 * 3
    # => x2 == 7.5
    # Same for y but multiplied with factor 10.

    x, y = pos[0]
    assert math.isclose(x, 7.5)
    assert math.isclose(y, 75)


def test_sparse_solver():
    A = sparse.csc_matrix([[1, 1, 2], [1, 3, 0], [2, 0, 0]], dtype=np.float)
    b = np.array([1, 2, 3])

    x, n_iter = linalg.cg(A, b)

    assert np.allclose(A.dot(x), b)
