#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from typing import Tuple, Union
from math import hypot

"""
Quad-tree (or Barnes-Hutt tree) implementation for efficient approximate calculations of electrostatic fields.
"""


class Vector2D:

    def __init__(self, x: float = 0, y: float = 0):
        self.x = x
        self.y = y

    def __sub__(self, other):
        return Vector2D(self.x - other.x, self.y - other.y)

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        return Vector2D(self.x * other, self.y * other)

    def __truediv__(self, other):
        return Vector2D(self.x / other, self.y / other)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y
        return self

    def __imul__(self, other):
        self.x *= other
        self.y *= other
        return self

    def __itruediv__(self, other):
        self.x /= other
        self.y /= other
        return self

    def abs(self):
        return hypot(self.x, self.y)

    def abs_squared(self):
        return self.x ** 2 + self.y ** 2

    def normalized(self):
        return self / self.abs()

    def __iter__(self):
        return iter((self.x, self.y))

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


class Particle:

    def __init__(self, location: Union[Vector2D, Tuple[float, float]], charge: float = 1):
        if not isinstance(location, Vector2D):
            location = Vector2D(*location)

        assert isinstance(location, Vector2D)
        self.location = location
        self.charge = charge

    def force_from(self, other_particle) -> Vector2D:
        """
        Calculate force that this particle feels coming from `other_particle`.
        :param other_particle:
        :return:
        """

        d = self.location - other_particle.location

        if d.x == 0 and d.y == 0:
            return Vector2D(0, 0)
        else:
            d_abs = d.abs()
            f = d / d_abs ** 3 * self.charge * other_particle.charge
            return f


class QuadTree:

    def __init__(self, lower_left: Union[Vector2D, Tuple[float, float]], side_length: float):
        self.lower_left = lower_left
        self.side_length = side_length
        self.child = None
        self.center_of_charge = None

    def update_center_of_charge(self):
        """
        Recursively compute center of charge for this tree and its subtrees.
        :return:
        """
        if self.child is None:
            self.center_of_charge = None
        elif isinstance(self.child, Particle):
            self.center_of_charge = self.child
        else:
            center = Vector2D(0, 0)
            charge = 0
            for subtree in self.child:
                if subtree is not None:
                    subtree.update_center_of_charge()
                    sub_loc = subtree.center_of_charge.location
                    assert isinstance(sub_loc, Vector2D)
                    sub_charge = subtree.center_of_charge.charge
                    center += sub_loc * sub_charge
                    charge += sub_charge
            if charge != 0:
                # If charge is zero then the location of the center does not matter anyway.
                center *= 1 / charge

            self.center_of_charge = Particle(center, charge=charge)

    def insert(self, particle: Particle):
        """
        Insert a particle into the tree structure.
        :param particle:
        :return:
        """
        if self.child is None:
            self.child = particle
        else:
            leaf = self.child
            if isinstance(self.child, Particle):

                if self.child.location == particle.location:
                    # Special case if the new particle has the exact same location as the existing.
                    # Then just sum up the charges but don't add a new particle.
                    self.child = Particle(self.child.location,
                                          self.child.charge + particle.charge)
                    return

                self.child = [None] * 4

            px, py = particle.location
            ox, oy = self.lower_left
            assert px >= ox
            assert py >= oy
            assert px - ox < self.side_length
            assert py - oy < self.side_length

            s_half = self.side_length / 2

            right_subtree = px >= ox + s_half
            upper_subtree = py >= oy + s_half

            index = self._get_subtree_index(right_subtree, upper_subtree)

            if self.child[index] is None:
                sub_ox = ox + s_half if right_subtree else ox
                sub_oy = oy + s_half if upper_subtree else oy
                subtree = QuadTree(lower_left=Vector2D(sub_ox, sub_oy), side_length=s_half)
                self.child[index] = subtree

            # Insert particle into subtree
            self.child[index].insert(particle)

            # If this node had a leaf, move it to a subtree.
            if isinstance(leaf, Particle):
                self.insert(leaf)

    def force_onto(self, particle: Particle, threshold=0.5) -> Vector2D:
        """
        Calculate the force onto the particle.
        :param particle:
        :param threshold: 0: least accurate but fastest.
        :return:
        """

        assert self.center_of_charge is not None, "Need to call `update_center_of_charge()` before!"

        if self.child is None:
            return Vector2D(0, 0)
        elif isinstance(self.child, Particle):
            return particle.force_from(self.child)
        else:
            total_force = Vector2D(0, 0)
            for subtree in self.child:
                if subtree is not None:
                    center = subtree.center_of_charge
                    dist = (particle.location - center.location).abs()

                    ratio = dist / self.side_length

                    if ratio > threshold:
                        total_force += particle.force_from(center)
                    else:
                        total_force += subtree.force_onto(particle, threshold=threshold)
            return total_force

    def _get_subtree_index(self, right: bool, upper: bool):
        return int(right) + int(upper) * 2


def test_particle_force():
    p1 = Particle((0, 0))
    p2 = Particle((1, 0))

    # Check if direction of force is correct.

    f = p1.force_from(p2)
    assert f.x < 0

    f = p2.force_from(p1)
    assert f.x > 0


def test_quadtree_insert():
    tree = QuadTree((0, 0), 1)

    p1 = Particle((0.1, 0.1))
    p2 = Particle((0.6, 0.2))
    p3 = Particle((0.2, 0.3))

    tree.insert(p1)
    tree.insert(p2)
    tree.insert(p3)

    print(tree.child)

    tree.update_center_of_charge()

    print(tree.center_of_charge)

    f = tree.force_onto(p1)
    print(f)


def test_quadtree_insert_random():
    from numpy import random
    random.seed(1)
    tree = QuadTree((0, 0), 1)

    locations = random.uniform(0, 1, size=(1000, 2))

    for x, y in locations:
        tree.insert(Particle((x, y)))

    tree.update_center_of_charge()
    print(tree.center_of_charge.location)

    p = Particle((0.75, 0.75))

    f = tree.force_onto(p, threshold=0.5)
    print(f)
