#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import networkx as nx
from ..klayout_helper import db
from typing import Dict, Tuple, Set
import logging

logger = logging.getLogger(__name__)


def netlist2graph(top_circuit: db.Circuit,
                  cell_areas: Dict[str, float],
                  fixed_positions: Dict[int, Tuple[int, int]]) -> Tuple[nx.MultiGraph, Set[str]]:
    """
    Returns a multigraph representing the connections between cells in the netlist and a set of virtual nodes that
    are in the graph but not in the netlist (center nodes for multi-terminal nets).
    :param top_circuit:
    :param cell_areas: Area for each cell.
    :param fixed_positions: Mapping of sub-circuit ID to a fixed `(x, y)` position.
    :return: (multi graph, set of virtual nodes)
    """
    # Construct multi graph.
    # Use star topology for multi terminal nets.
    connection_graph = nx.MultiGraph()

    # Create graph nodes.
    for subcircuit in top_circuit.each_subcircuit():
        assert isinstance(subcircuit, db.SubCircuit)
        cell_name = subcircuit.circuit_ref().name

        if cell_name in cell_areas:
            area = cell_areas[cell_name]
        else:
            area = 0
            logger.warning('No area defined for cell: {}'.format(cell_name))

        if subcircuit.id() in fixed_positions:
            pos = fixed_positions[subcircuit.id()]
            connection_graph.add_node(subcircuit.id(), area=area, fixed=True, position=pos)
        else:
            connection_graph.add_node(subcircuit.id(), area=area)

    virtual_nodes = set()
    # Insert graph edges based on netlist.
    for net in top_circuit.each_net():
        assert isinstance(net, db.Net)
        num_terminals = net.subcircuit_pin_count()

        # Find all sub-circuits that are connected to this net.
        instances = set()
        for subcircuit_pin in net.each_subcircuit_pin():
            subcircuit = subcircuit_pin.subcircuit()
            subcircuit = top_circuit.subcircuit_by_id(subcircuit.id())
            instances.add(subcircuit)

        if num_terminals == 2:
            # For a 2-terminal net create a single edge.
            a, b = instances
            assert isinstance(a, db.SubCircuit)
            assert isinstance(b, db.SubCircuit)
            a = a.id()
            b = b.id()
            # TODO: set weight based on some timing information. Higher weights for more critical paths.
            connection_graph.add_edge(a, b, net, weight=1, label=net)
        elif num_terminals > 2:
            # For a multi-terminal net create a star network.
            node_name = "virtual_center_{}".format(net.name)
            virtual_nodes.add(node_name)
            for m in instances:
                assert isinstance(m, db.SubCircuit)
                m = m.id()
                # TODO: set weight based on some timing information. Higher weights for more critical paths.
                connection_graph.add_edge(m, node_name, net, weight=1, label=net)

    # Check if there are any unconnected.
    # This could indicate that something during the netlist reading/conversion went wrong.
    nodes_without_connection = [n for n in connection_graph.nodes if len(connection_graph[n]) == 0]
    if len(nodes_without_connection) > 0:
        logger.warning("There are {} unconnected nodes.".format(len(nodes_without_connection)))
        logger.warning('Nodes without connection ({}): {}'.format(len(nodes_without_connection),
                                                                  [top_circuit.subcircuit_by_id(n).name for n in
                                                                   nodes_without_connection]))

    return connection_graph, virtual_nodes
