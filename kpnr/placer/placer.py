#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import networkx as nx
from ..klayout_helper import db
import numpy as np
from typing import Tuple, Dict, Any, Set, Optional
from . import netlist2graph
import logging

logger = logging.getLogger(__name__)


def relabel_nodes_with_integers(graph: nx.MultiGraph) -> Tuple[nx.MultiGraph, Dict[int, Any]]:
    """
    Rename nodes such that their ID is a int.
    :param graph:
    :return: Relabeled graph and dictionary with the reverse mapping.
    """
    mapping = {
        n: i + 1 for i, n in enumerate(graph.nodes)
    }

    relabeled = nx.relabel_nodes(graph, mapping, copy=True)

    reverse_mapping = {
        i: n for n, i in mapping.items()
    }
    return relabeled, reverse_mapping


class StdCellPlacer:

    def __init__(self):
        self._convert_nodes_to_integers = False

    def enable_convert_nodes_to_integers(self, enable: bool):
        """
        Tells if node indices of graph must be integers.
        :return:
        """
        self._convert_nodes_to_integers = enable

    def place(self,
              circuit: db.Circuit,
              core_area: db.DSimplePolygon,
              shapes: Dict[str, Tuple[int, int]],
              positions: Optional[Dict[int, Tuple[int, int]]] = None,
              fixed_circuits: Optional[Set[int]] = None,
              ) -> Dict[int, Tuple[int, int]]:
        """
        Indirectly calls `place_impl` after doing some sanity tests.

        Netlist is expected to be given as a `networkx.MultiGraph` where each node represents a standard-cell instance
        with attributes 'fixed' (True iff this node has a fixed position), 'position' (optional as initial position hint),
        'area' (area of standard-cell).

        Edges are expected to have a 'weight' attribute.
        :param circuit: The circuit to be placed.
        :param core_area: A polygon representing the area where cells are allowed to be placed.
        :param shapes: A dictionary mapping circuits to their shapes (width, height).
        :param positions: A dictionary mapping circuit instances to initial positions.
        :param fixed_circuits: Set of cell instance IDs which have a fixed position.
        :return:
        """
        if positions is None:
            positions = dict()
        if fixed_circuits is None:
            fixed_circuits = set()

        assert isinstance(circuit, db.Circuit)
        assert isinstance(shapes, Dict)
        assert isinstance(positions, Dict)
        assert isinstance(fixed_circuits, Set)

        assert all((isinstance(id, str) for id, (x, y) in shapes.items()))
        assert all((isinstance(id, int) for id, (x, y) in positions.items()))
        assert all((isinstance(id, int) for id in fixed_circuits))

        cell_area = {k: w * h for k, (w, h) in shapes.items()}
        fixed_positions = {k: v for k, v in positions.items() if k in fixed_circuits}
        graph, virtual_nodes = netlist2graph.netlist2graph(circuit, cell_area, fixed_positions)

        # Sanity checks on node data.
        for n, data in graph.nodes(data=True):
            if data.get('fixed', False):
                assert 'position' in data, '`position` must be given for `fixed` nodes.'

        if self._convert_nodes_to_integers:
            logging.info("Convert node indices to integers.")
            # Transparently convert node indices into integers.
            graph, reverse_map = relabel_nodes_with_integers(graph)
            positions, int_indices = self.place_impl(graph, core_area)
            # Convert node indices back to original values.
            indices = [reverse_map[i] for i in int_indices]
        else:
            positions, indices = self.place_impl(graph, core_area)

        pos = {i: (x, y) for i, (x, y) in zip(indices, positions) if i not in virtual_nodes}

        return pos

    def place_impl(self, graph: nx.MultiGraph, core_area: db.DSimplePolygon) -> Tuple[np.ndarray, np.ndarray]:
        """
        Place the standard cells.
        :param graph:
        :return:
        """
        raise NotImplementedError()
