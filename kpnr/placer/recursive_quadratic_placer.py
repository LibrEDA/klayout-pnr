#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np

import networkx as nx

from ..placer.quadratic_placer import place_quadratic
from ..placer.util import _get_fixed_node_indices

import logging

logger = logging.getLogger(__name__)


def test_recursive_place_quadratic():
    import matplotlib.pyplot as plt
    g = nx.MultiGraph()
    g.add_node(0, position=(0, 0), fixed=True)
    g.add_node(1, position=(10, 10), fixed=True)
    g.add_node(2, position=(10, 0), fixed=True)
    g.add_node(3, position=(0, 10), fixed=True)
    # g.add_edges_from([
    #     (0,2),
    #     (2,1)
    # ])

    num_cells = 200

    # Connect cells serially
    r = np.arange(num_cells)
    edges = np.array([r, (r + 1) % num_cells]).T
    g.add_edges_from(edges)

    # Add some random edges
    num_random_edges = 40
    np.random.seed(1)
    edges = np.random.randint(0, num_cells, (num_random_edges, 2))
    g.add_edges_from(edges)

    assert nx.is_connected(g)

    solution, idx = place_quadratic(g)

    pos = dict()
    for i, (x, y) in zip(idx, solution):
        pos[i] = (x, y)

    fixed_idx = _get_fixed_node_indices(g)
    positions = nx.get_node_attributes(g, 'position')
    for i in fixed_idx:
        pos[i] = positions[i]

    nx.draw_networkx(g, pos, with_labels=True)
    plt.show()
