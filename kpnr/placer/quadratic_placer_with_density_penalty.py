#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from ..placer import quadratic_placer, quadtree as qt, util
from typing import Tuple, Set
import networkx as nx
import numpy as np
import itertools
from ..klayout_helper import db
from .placer import StdCellPlacer


class QuadraticDensityPlacer(StdCellPlacer):
    """
    Uses QuadraticPlacer internally and adds virtual nodes that have the effect of pulling standard-cell nodes
    away from dense regions.
    """

    def __init__(self, density_penalty: float):
        """

        :param density_penalty: Tells how strong the cells repulse each other.
        """
        # Altorithm requires the graph nodes to be integers.
        self.enable_convert_nodes_to_integers(True)
        self.density_penalty = density_penalty

    def place_impl(self, graph: nx.MultiGraph, core_area: db.DSimplePolygon) -> Tuple[np.ndarray, np.ndarray]:
        return self._place(graph, core_area)

    def _place(self, graph: nx.MultiGraph, core_area: db.DSimplePolygon, cell_area_keyword='area',
               default_cell_area=1) -> Tuple[
        np.ndarray, np.ndarray]:
        # Get lookup-table for cell areas.
        cell_sizes = nx.get_node_attributes(graph, cell_area_keyword)

        isfixed = nx.get_node_attributes(graph, 'fixed')

        # Find bounding box of all fixed nodes.
        positions = nx.get_node_attributes(graph, 'position')

        fixed_indices = util._get_fixed_node_indices(graph)
        fixed_positions = np.array([positions[i] for i in fixed_indices])
        min_x = np.min(fixed_positions[:, 0])
        max_x = np.max(fixed_positions[:, 0])
        min_y = np.min(fixed_positions[:, 1])
        max_y = np.max(fixed_positions[:, 1])

        # Find dimensions of quad tree such that all fixed nodes fit inside.
        sidelength = max(max_x - min_x, max_y - min_y) * 1.1

        # Find placement.
        solution_xy, idx = quadratic_placer.place_quadratic(graph)

        # Convert positions into format.
        pos = {i: (x, y) for i, (x, y) in zip(idx, solution_xy)}

        # Calculate density penalty.
        tree = qt.QuadTree(lower_left=qt.Vector2D(min_x - sidelength * 0.05, min_y - sidelength * 0.05),
                           side_length=sidelength)
        # Populate the quad tree
        for i, (x, y) in pos.items():
            area = cell_sizes.get(i, default_cell_area)
            tree.insert(qt.Particle(qt.Vector2D(x, y), charge=area))

        # Pre-calculate potential.
        tree.update_center_of_charge()

        # Insert virtual nodes
        virtual_node_index_start = max(idx) + 1
        node_indices = itertools.count(virtual_node_index_start)
        # Keep track of virtual nodes.
        virtual_nodes_indices = []

        # Create an independent copy.
        graph_with_penalty = graph.copy()
        assert isinstance(graph_with_penalty, nx.MultiGraph)

        # Evaluate gradient of penalty function (here this is the electro-static force).
        # ... and add virtual fixed nodes that pull the cells towards less dense regions.
        for i, (x, y) in pos.items():
            area = cell_sizes.get(i, default_cell_area)
            f = tree.force_onto(qt.Particle(qt.Vector2D(x, y), charge=area))
            f = db.DVector(f.x, f.y)
            if f.length() > 0:
                virtual_anchor_node = next(node_indices)
                virtual_nodes_indices.append(virtual_anchor_node)

                location = db.DPoint(x, y)

                # Get position of anchor node such that the resulting spring force between node i and the anchor node
                # is proportional to `f`.

                # Intersect with core boundary.
                intersection = _ray2polygon_intersection(location, f, core_area)

                assert len(intersection) == 1

                anchor_location = intersection.pop()

                distance = anchor_location.distance(location)
                if distance > 0:
                    weight = 1 / float(distance) * self.density_penalty  # TODO: normalize properly
                    (x, y) = anchor_location.x, anchor_location.y
                    graph_with_penalty.add_node(virtual_anchor_node, fixed=True, position=(x, y))
                    graph_with_penalty.add_edge(i, virtual_anchor_node, weight=weight)

        # Find placement.
        solution_xy, idx = quadratic_placer.place_quadratic(graph_with_penalty)

        # Strip away virtual nodes.
        solution_xy = solution_xy[idx < virtual_node_index_start]
        idx = idx[idx < virtual_node_index_start]

        return solution_xy, idx


def _ray2polygon_intersection(ray_start: db.DPoint,
                              ray_direction: db.DVector,
                              polygon: db.DSimplePolygon) -> Set[db.DPoint]:
    """
    Find intersections points of the ray with polygons.
    :param ray_start:
    :param ray_direction:
    :param region:
    :return:
    """
    assert isinstance(polygon, db.DSimplePolygon)
    assert isinstance(ray_start, db.DPoint)
    assert isinstance(ray_direction, db.DVector)

    bbox = polygon.bbox()
    bbox_size = max(bbox.width(), bbox.height()) * 2

    ray = db.DEdge(ray_start, ray_start + ray_direction / ray_direction.length() * bbox_size)
    intersections = set()
    for edge in polygon.each_edge():
        if ray.intersect(edge):
            p = ray.intersection_point(edge)
            intersections.add(p)
    return intersections
