#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typing import Dict, Tuple
import itertools
import networkx as nx
from ..placer import quadratic_placer, util
import logging

logger = logging.getLogger(__name__)


def split_graph(graph: nx.Graph, pos: Dict, axis: int) -> Tuple[nx.Graph, nx.Graph, float]:
    """
    Split nodes in x or y direction into equal sized subgraphs.
    :param graph:
    :param pos: Dictionary containing positions of nodes Dict[index, (x,y)]
    :param axis: 0: split in x direction, 1: split in y direction
    :return: Return both resulting sub-graphs and the offset of the cut-line (lower subgraph, upper subgraph, cut-line)
    """

    # Sort indices and positions by x or y coordinate.
    positions = [it for it in pos.items() if it[0] in graph]
    positions.sort(key=lambda k: k[1][axis])

    # Split in half.
    indices = [i for i, p in positions]
    middle = len(indices) // 2
    idx_lower = indices[:middle]
    idx_upper = indices[middle:]

    # Create subgraphs.
    lower_subgraph = nx.subgraph(graph, idx_lower)
    upper_subgraph = nx.subgraph(graph, idx_upper)

    # Get offset of cut-line (line which separates the both sub-graphs)
    lower = pos[idx_lower[-1]][axis]  # offset of right-most element in left partition
    upper = pos[idx_upper[0]][axis]  # offset of left-most element in right partition
    cut_line = (lower + upper) / 2

    assert all([pos[i][axis] <= cut_line for i in idx_lower])
    assert all([pos[i][axis] >= cut_line for i in idx_upper])

    return lower_subgraph, upper_subgraph, cut_line


def place_recursive(graph: nx.MultiGraph, axis=0) -> Dict:
    assert axis in [0, 1], "Axis must be 0 for x or 1 for y."
    assert isinstance(graph, nx.MultiGraph)
    # Find placement.
    solution_xy, idx = quadratic_placer.place_quadratic(graph)

    # Convert positions into format.
    pos = {i: (x, y) for i, (x, y) in zip(idx, solution_xy)}

    # TODO: abort recursion if maximal density is low enough
    if len(graph) < 20:
        return pos

    # Create subgraphs
    subgraph_left, subgraph_right, cut_line = split_graph(graph, pos, axis=axis)
    print('cut_line = ', cut_line)

    # Create independent copies.
    subgraph_left = subgraph_left.copy()
    subgraph_right = subgraph_right.copy()

    assert len(subgraph_left) > 0
    assert len(subgraph_right) > 0

    # Plot
    # import matplotlib.pyplot as plt
    # nx.draw_networkx(subgraph_left, pos, with_labels=True, node_color='blue')
    # nx.draw_networkx(subgraph_right, pos, with_labels=True, node_color='red')
    # plt.show()

    # Find edges that used to connect the subgraphs.
    crossing_edges = graph.edges - subgraph_left.edges - subgraph_right.edges

    # Insert virtual nodes
    max_node_index = max(idx)
    node_indices = itertools.count(max_node_index + 1)
    # Keep track of virtual nodes.
    virtual_nodes_indices = []

    # Insert virtual nodes
    assert isinstance(graph, nx.MultiGraph)
    for n1, n2, _index in crossing_edges:
        assert (n1 in subgraph_left) ^ (n1 in subgraph_right)
        assert (n2 in subgraph_left) ^ (n2 in subgraph_right)
        assert (n1 in subgraph_left) ^ (n2 in subgraph_left)

        if n1 in subgraph_left:
            n_left, n_right = n1, n2
        else:
            n_left, n_right = n2, n1

        # Get original weight of the edge.
        weight = graph.get_edge_data(n_left, n_right, 'weight')
        if weight is None:
            weight = 1

        n_virtual = next(node_indices)
        virtual_nodes_indices.append(n_virtual)

        # Find position of virtual node.
        # It is placed on the intersection point of the edge n1-n2 and the cut-line separating the both subgraphs.
        x_min = min([p[axis] for p in pos.values()])
        x_max = max([p[axis] for p in pos.values()])
        x = (x_min + x_max) / 2
        print('x = ', x)
        other_axis = 0 if axis == 1 else 1
        x_left = pos[n_left][axis]
        x_right = pos[n_right][axis]
        y_left = pos[n_left][other_axis]
        y_right = pos[n_right][other_axis]
        y = (y_right * (cut_line - x_left) + y_left * (x_right - cut_line)) / (x_right - x_left)

        pos_virtual = (x, y) if axis == 0 else (y, x)

        pos[n_virtual] = pos_virtual

        # Add virtual nodes.
        subgraph_left.add_node(n_virtual, position=pos_virtual, fixed=True)
        subgraph_right.add_node(n_virtual, position=pos_virtual, fixed=True)

        subgraph_left.add_edge(n_left, n_virtual, weight=weight)
        subgraph_right.add_edge(n_right, n_virtual, weight=weight)

    # Dive into the recursion.
    new_axis = 0 if axis == 1 else 1
    pos_left = place_recursive(subgraph_left, new_axis)
    pos_right = place_recursive(subgraph_right, new_axis)

    # Merge results.
    pos = dict()
    pos.update(pos_left)
    pos.update(pos_right)

    for n in subgraph_left.nodes:
        assert n in pos
    for n in subgraph_right.nodes:
        assert n in pos
    for n in graph.nodes:
        assert n in pos

    # TODO: Strip positions of virtual nodes.

    return pos


def test_place_quadratic():
    import matplotlib.pyplot as plt

    # Generate dummy graph.
    graph = util._generate_test_graph(num_nodes=50, num_random_edges=10)

    pos = place_recursive(graph, axis=0)

    # Plot
    nx.draw_networkx(graph, pos, with_labels=True, node_color='blue')
    plt.show()
