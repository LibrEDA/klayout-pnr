#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from .test_data import *
from .netlist import util as net_util
import logging

logger = logging.getLogger(__name__)


def test_verilog_netlist():
    """
    1) Read a verilog netlist and a liberty library.
    2) Convert the netlist into a networkx.MultiGraph structure.
    3) Feed the MultiGraph to the QuadraticPlacer
    4) Plot the result.
    :return:
    """

    library = parse_liberty(get_example_liberty_library())

    liberty_reader = LibertyNetlistReader()

    leaf_cells = liberty_reader.read_liberty(library)

    klayout_verilog_reader = VerilogNetlistReader(leaf_circuits=leaf_cells)
    klayout_netlist = klayout_verilog_reader.read_netlist(get_example_verilog_netlist())

    top_circuit: db.Circuit = next(klayout_netlist.each_circuit_top_down())
    print("Top circuit: {}".format(top_circuit.name))

    logger.info("Top circuit: {}".format(top_circuit.name))

    net_util.flatten(klayout_netlist)

    num_cells = len(list(top_circuit.each_subcircuit()))
    logger.info('Number of cells: {}'.format(num_cells))
    print(num_cells)
