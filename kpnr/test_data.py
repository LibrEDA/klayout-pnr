#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Helper functions to fetch test and example data such as netlists and cell libraries.
"""

import os
from .klayout_helper import db

from liberty.parser import parse_liberty
import liberty.types

from .netlist.verilog_reader import VerilogNetlistReader
from .netlist.liberty_reader import LibertyNetlistReader


def get_path(file_name: str) -> str:
    """
    Get absolute path to a file in the `test_data` directory.
    :param filename:
    :return:
    """
    _path = '../test_data/{}'.format(file_name)
    return os.path.join(os.path.dirname(__file__), _path)


def get_example_verilog_netlist(file_name: str = 'seq_chip_nl.v') -> str:
    """
    Get content of an example Verilog netlist.
    :return:
    """
    verilog_file = get_path(file_name)
    with open(verilog_file) as f:
        data = f.read()
    return data


def get_example_liberty_library(file_name: str = 'gscl45nm.lib') -> str:
    """
    Get content of an example liberty library.
    :return:
    """
    path = get_path(file_name)
    with open(path) as f:
        data = f.read()
    return data


def get_example_liberty() -> liberty.types.Group:
    """
    Get parsed liberty library.
    :return:
    """
    library = parse_liberty(get_example_liberty_library())
    return library


def get_example_netlist(file_name: str = 'seq_chip_nl.v') -> db.Netlist:
    """
    Get fully parsed netlist.
    :return:
    """
    library = get_example_liberty()
    liberty_reader = LibertyNetlistReader()

    leaf_cells = liberty_reader.read_liberty(library)

    klayout_verilog_reader = VerilogNetlistReader(leaf_circuits=leaf_cells)
    klayout_netlist = klayout_verilog_reader.read_netlist(get_example_verilog_netlist(file_name))

    return klayout_netlist
