#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Data-structure for representing a simple digital macro block throughout the place-and-route flow.
"""

from .klayout_helper import db

from typing import Dict


class SimpleDesign:
    """
    Design of a simple digital macro block which consists of standard-cells only.
    """

    def __init__(self):
        self.layout: db.Layout = None
        self.netlist: db.Netlist = None
        # The circuit which describes the connectivity of the standard-cells.
        self.top_circuit: db.Circuit = None
        # The top cell of this macro block.
        self.top_cell: db.Cell = None
        # The area where standard-cells should be placed.
        self.core_area: db.DSimplePolygon = None

        # Mapping between layout cells and netlist cells.
        self.cell_instances_by_subcircuit_id: Dict[int, db.CellInstArray] = dict()

