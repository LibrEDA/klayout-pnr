#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typing import *
from ..klayout_helper import db

from .legalizer import Legalizer
import numpy as np


class TetrisLegalizer(Legalizer):
    """Simple-stupid legalizer implementation.

    Roughly, the algorithm implemented here sorts cells by x-coordinates and then takes one after the other and
    tries to fit them onto the closest row. Rows are filled from left to right.
    """

    def __init__(self, row_height: int, x_grid_pitch: int):
        """
        :param row_height: Height of the standard-cell rows.
        :param x_grid_pitch:
        """
        super().__init__()
        assert row_height > 0
        self.row_height = row_height
        self.x_grid_pitch = x_grid_pitch

    def legalize_impl(self,
                      circuit: db.Circuit,
                      core_area: db.DPolygon,
                      cell_shapes: Dict[str, Tuple[int, int]],
                      positions: Dict[int, Tuple[int, int]],
                      fixed_circuits: Optional[Set[int]] = None,
                      ) -> Dict[int, db.Trans]:
        assert isinstance(core_area, db.DBox), "Core area must be a box. Polygons are not supported."

        row_height = self.row_height
        num_rows = int(core_area.height() // row_height)
        row_width = int(core_area.width())
        max_x = int(core_area.p2.x)

        grid_x = self.x_grid_pitch

        row_length = np.zeros(num_rows)

        # y-coordinate of rows
        row_ys = np.arange(num_rows) * row_height + core_area.p1.y

        all_cells = list(positions.keys())
        # Sort by x-coordinate.
        all_cells.sort(key=lambda cell: positions[cell][0])

        def get_row_idx(y: int):
            idx = (y - core_area.p1.y) // row_height
            return max(0, min(num_rows - 1, idx))

        # Dictionary of mapping a cell ID to a geometric transformation.
        transformations = dict()

        # Play Tetris and find positions and rotations for the cells.
        for cell in all_cells:
            x_orig, y_orig = positions[cell]
            shape = cell_shapes.get(circuit.subcircuit_by_id(cell).circuit_ref().name, (0, row_height))
            w, h = shape
            assert h == row_height, "Cell must have the same height as the standard-cell row!"

            # Find rows that are already full.
            full_rows = row_length + w >= row_width

            # Get best x positions for every row.
            possible_x_positions = np.maximum(row_length + core_area.p1.x, x_orig)
            # Round up to next grid position.
            possible_x_positions = ((possible_x_positions + grid_x - 1) // grid_x) * grid_x

            distances_squared = (0 - x_orig) ** 2 + (row_ys - y_orig) ** 2
            # Make values large for full rows such that they are not chosen.
            distances_squared[full_rows] = np.max(distances_squared) + 1
            best_row_index = np.argmin(distances_squared)

            assert not full_rows[best_row_index], "Ahhh... Cell placed into full row."

            # best_row_index = get_row_idx(y_orig)

            x = max(possible_x_positions[best_row_index], x_orig)
            x = min(x, max_x - w)

            # Round up to next grid position.
            x = ((x + grid_x - 1) // grid_x) * grid_x

            row_length[best_row_index] = x + w

            x = int(x)
            y = int(best_row_index * row_height)

            # Make sure that cells face each other always nwell-to-nwell and pwell-to-pwell.
            # So cells in every second row must be rotated by 180 degrees (or alternatively mirrored).
            fliprow = (best_row_index % 2) == 1
            if fliprow:
                rotation = 2
                x += w
                y += h
            else:
                rotation = 0
            mirr = False
            transformations[cell] = db.Trans(rotation, mirr, x, y)

        return transformations
