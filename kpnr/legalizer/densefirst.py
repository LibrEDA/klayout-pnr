#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typing import *
from ..klayout_helper import db

from .legalizer import Legalizer
import numpy as np
from collections import defaultdict


class DenseFirstLegalizer(Legalizer):
    """
    Greedy legalization algorithm.
    Each cell is placed to its closest legal position. Cells in dense regions are placed first.
    """

    def __init__(self, row_height: int, x_grid_pitch: int):
        """
        :param row_height: Height of the standard-cell rows.
        :param x_grid_pitch: Pitch of cell alignment grid in x-direction.
        """
        super().__init__()
        assert row_height > 0
        self.row_height = row_height
        self.x_grid_pitch = x_grid_pitch

    def legalize_impl(self,
                      circuit: db.Circuit,
                      core_area: db.DPolygon,
                      cell_shapes: Dict[str, Tuple[int, int]],
                      positions: Dict[int, Tuple[int, int]],
                      fixed_circuits: Optional[Set[int]] = None,
                      ) -> Dict[int, db.Trans]:
        assert isinstance(core_area, db.DBox), "Core area must be a box. Polygons are not supported."

        core_area = core_area.to_itype().bbox()

        if fixed_circuits is None:
            # Create default: Empty set.
            fixed_circuits = set()

        row_height = self.row_height
        # Find number of standard-cell rows.
        num_rows = int(core_area.height() // row_height)
        row_width = int(core_area.width())
        max_x = int(core_area.p2.x)
        grid_x = self.x_grid_pitch

        # Get y-coordinates of the rows.
        row_y_coord = np.arange(num_rows) * row_height + core_area.p1.y

        # Get list of all cell indices to be placed.

        all_instances = list((k for k in positions.keys() if k not in fixed_circuits))

        # Sort by x-coordinate.
        all_instances.sort(key=lambda cell: positions[cell][0])

        # Get all cell boundaries.
        cell_boxes = dict()
        for inst_id in all_instances:
            circuit_name = circuit.subcircuit_by_id(inst_id).circuit_ref().name
            shape = cell_shapes.get(circuit_name, (0, row_height))
            w, h = shape
            cell_boxes[inst_id] = db.Box(db.Point(0, 0), db.Point(w, h))

        # Size of a bin used to compute the average density.
        bin_size = row_height * 4

        def bin_indices(x, y) -> Tuple[int, int]:
            """
            Convert coordinates of cells into bin indices.
            """
            i = (x - core_area.p1.x) // bin_size
            j = (y - core_area.p1.y) // bin_size
            return int(i), int(j)

        def bin_shape(i, j) -> db.Box:
            # Get the rectangular boundary of the bin by its index.
            p0 = core_area.p1 + db.Vector(i, j) * bin_size
            p1 = p0 + db.Vector(bin_size, bin_size)
            return db.Box(p0, p1)

        # Cells put into the bin which intersects with their lower left corner.
        bins = defaultdict(set)
        # Total area of cells overlapping with the bin.
        bin_content = defaultdict(lambda: 0)

        # Limit the global positions to the core area.
        cell_positions = dict()
        ll = core_area.p1
        ul = core_area.p2
        for id, (x, y) in positions.items():
            x_lim = min(max(x, ll.x), ul.x)
            y_lim = min(max(y, ll.y), ul.y)
            cell_positions[id] = (x_lim, y_lim)

        # Put cells into bins.
        # Accumulate the overlapping area with the bin to find the cell density.
        for inst in all_instances:
            x, y = cell_positions[inst]
            # Create boundary of the cell at the right position.
            cell_box = cell_boxes[inst].transformed(db.Trans(x, y))
            assert isinstance(cell_box, db.Box)
            assert cell_box.width() < bin_size
            assert cell_box.height() < bin_size

            # Update all bins that overlap with the cell.
            # TODO: This method only works if the cell is smaller than the bin.
            for p in db.SimplePolygon(cell_box).each_point():
                i, j = bin_indices(p.x, p.y)
                bin_box = bin_shape(i, j)
                overlap: db.Box = bin_box & cell_box  # Compute intersection.
                if not overlap.empty():
                    # Remember which instance is intersecting with the bin.
                    bins[(i, j)].add(inst)
                    # Accumulate the density.
                    bin_content[(i, j)] += overlap.area()

        # Store the unused space in each row as intervals of the form (start, end).
        # Initialize the intervals with the full width of the core.
        rows_free = [[(core_area.p1.x, core_area.p2.x)] for _ in range(num_rows)]

        def find_best_cell_position_x_in_interval(pos_x: int, cell_width: int, bounds: Tuple[int, int]) \
                -> Optional[int]:
            # Find the best x-coordinate to place a cell within an interval.
            if bounds[1] - bounds[0] >= cell_width:
                return min(max(bounds[0], pos_x), bounds[1] - cell_width)
            else:
                return None

        def find_best_cell_position_x_in_row(row: List[Tuple[int, int]], x: int, cell_width: int) \
                -> Optional[Tuple[int, int]]:
            # Find the a free position in the row that is closest to `x`.
            # Return the index of the interval and the found x position if there is any.
            best_pos_local = (
                (i, find_best_cell_position_x_in_interval(x, cell_width, interval))
                for i, interval in enumerate(row)
            )

            # Strip away `None`s.
            best_pos_local = ((i, v) for i, v in best_pos_local if v is not None)
            # Find the closest position among all intervals.
            best_x = min(best_pos_local, default=None, key=lambda ipos: abs(ipos[0] - x))
            return best_x

        def insert_cell_into_row(row: List, interval_idx: int, pos_x: int, cell_width: int):
            # Remove the space used by the cell from the row.
            interval = row[interval_idx]
            assert interval[0] <= pos_x
            assert pos_x + cell_width <= interval[1]

            if interval[0] < pos_x and pos_x + cell_width < interval[1]:
                # Interval needs to be split in two intervals.
                lower = (interval[0], pos_x)
                upper = (pos_x + cell_width, interval[1])
                row[interval_idx] = lower  # Replace the old interval.
                row.insert(interval_idx + 1, upper)  # Insert the new interval.
            elif interval[0] == pos_x and interval[1] == pos_x + cell_width:
                # Cell spans the full interval.
                # Remove the interval.
                del row[interval_idx]
            elif interval[0] == pos_x:
                # Cell is positioned at the start of the interval.
                # Shorten the interval.
                row[interval_idx] = (pos_x + cell_width, interval[1])
            elif interval[1] == pos_x + cell_width:
                # Cell is positioned at the end of the interval.
                # Shorten the interval.
                row[interval_idx] = (interval[0], pos_x)

        def find_best_cell_positions(rows: List[List],
                                     row_y_coord: List[int],
                                     pos: Tuple[int, int],
                                     cell_width: int) \
                -> Optional[Tuple[Tuple[int, int], Tuple[int, int]]]:
            """
            Find the free position that is closest to the global position of the cell.
            Return a tuple (best free position, (row index, interval index)).
            """
            # TODO: Optimize. There's no need to search all the rows.
            # TODO: Start width the closest row then expand from there in both y directions.
            # TODO: Stop search as soon as the distance from the current row to the global position is larger that the best found position.
            pos_x, pos_y = pos
            best_x = ((i, find_best_cell_position_x_in_row(row, pos_x, cell_width)) for i, row in enumerate(rows))
            best_x = ((i, x) for i, x in best_x if x is not None)

            # Find the closest fit by euclidean distance.
            def dist(t):
                i, (_j, x) = t
                y = row_y_coord[i]
                return (x - pos_x) ** 2 + (y - pos_y) ** 2

            closest = min(best_x, default=None, key=dist)

            if closest is not None:
                i, (j, x) = closest
                y = row_y_coord[i]
                return (x, y), (i, j)
            else:
                return None

        # Sort bins by density.
        bins_by_density_ascending = sorted(bins, key=lambda bin_idx: bin_content[bin_idx])

        # For each cell find the density around it.
        density_around_cell = {
            cell: bin_content[bin_idx]
            for bin_idx in bins_by_density_ascending
            for cell in bins[bin_idx]
        }

        # Sort the cells by descending density.
        cells_by_density_descending = sorted(all_instances, key=lambda id: density_around_cell[id],
                                             reverse=True)
        # Associate circuit instances with transformations that describe their location and orientation.
        transformations = dict()

        # Place cells in high-density regions first.
        for cell in cells_by_density_descending:
            pos = cell_positions[cell]
            cell_box = cell_boxes[cell]
            cell_width = cell_box.width()
            cell_height = cell_box.height()

            best_pos = find_best_cell_positions(rows_free, row_y_coord, pos, cell_width)

            if best_pos is None:
                raise Exception("No free space: Failed to find legal positions for all cells")

            (x, y), (i, j) = best_pos
            # Mark the position as used.
            insert_cell_into_row(rows_free[i], j, x, cell_width)

            # Make sure that cells face each other always nwell-to-nwell and pwell-to-pwell.
            # So cells in every second row must be rotated by 180 degrees (or alternatively mirrored).
            flip = i % 2 == 1
            if flip:
                rotation = 2
                disp_x = x + cell_width
                disp_y = y + cell_height
            else:
                rotation = 0
                disp_x, disp_y = x, y
            mirror = False

            trans = db.Trans(rotation, mirror, int(disp_x), int(disp_y))

            transformations[cell] = trans

        return transformations
