#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from typing import *
from ..klayout_helper import db


class Legalizer():

    def __init__(self):
        pass

    def legalize(self,
                 circuit: db.Circuit,
                 core_area: db.DPolygon,
                 cell_shapes: Dict[str, Tuple[int, int]],
                 positions: Dict[int, Tuple[int, int]],
                 fixed_circuits: Optional[Set[int]] = None,
                 ) -> Dict[int, db.Trans]:
        """
        Find `db.Trans` transformations that put each cell into a legal position.
        :param circuit: The netlist.
        :param core_area: A polygon defining where cells are allowed to be placed.
        :param positions: Rough positions of the cells given by global placement. Indexed by the circuit IDs of the netlist `circuit`.
        :param cell_shapes: (width, height) tuples of the cells indexed by the cell name (such as INVX1, ...)
        :param fixed_circuits: A set of subcircuit IDs that have a fixed position
         which should not be changed during legalization.
        :return: Returns a dictionary which maps circuit IDs to transformations that place the cells on legal positions. `{circuit_id: db.Trans, ...}`
        """

        # Set default value for optional arguments.
        if fixed_circuits is None:
            fixed_circuits = set()

        # Check that inputs have the right types.
        assert isinstance(circuit, db.Circuit)
        assert isinstance(cell_shapes, Dict)
        assert isinstance(positions, Dict)
        assert isinstance(fixed_circuits, Set)

        # Check that keys of dictionaries have the expected types.
        assert all((isinstance(id, str) for id, (x, y) in cell_shapes.items()))
        assert all((isinstance(id, int) for id, (x, y) in positions.items()))
        assert all((isinstance(id, int) for id in fixed_circuits))

        return self.legalize_impl(
            circuit=circuit,
            core_area=core_area,
            cell_shapes=cell_shapes,
            positions=positions,
            fixed_circuits=fixed_circuits
        )

    def legalize_impl(self,
                      circuit: db.Circuit,
                      core_area: db.DPolygon,
                      cell_shapes: Dict[str, Tuple[int, int]],
                      positions: Dict[int, Tuple[int, int]],
                      fixed_circuits: Optional[Set[int]] = None,
                      ) -> Dict[int, db.Trans]:
        raise NotImplementedError()
