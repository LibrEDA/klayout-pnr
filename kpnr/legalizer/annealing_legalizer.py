#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typing import *
from ..klayout_helper import db

from .legalizer import Legalizer
import numpy as np


class AnnealingLegalizer(Legalizer):
    """
    Does not really work.
    """
    def __init__(self, row_height: int):
        super().__init__()
        assert row_height > 0
        self.row_height = row_height

    def legalize_impl(self,
                      circuit: db.Circuit,
                      core_area: db.DPolygon,
                      cell_shapes: Dict[str, Tuple[int, int]],
                      positions: Dict[int, Tuple[int, int]],
                      fixed_circuits: Optional[Set[int]] = None,
                      ) -> Dict[int, db.Trans]:
        assert isinstance(core_area, db.DBox)

        row_height = self.row_height
        num_rows = int(core_area.height() // row_height)
        row_width = int(core_area.width())
        min_x = int(core_area.p1.x)
        max_x = int(core_area.p2.x)

        min_y = int(core_area.p1.y)
        max_y = int(core_area.p2.x)

        row_length = np.zeros(num_rows)

        # y-coordinate of rows
        row_ys = np.arange(num_rows) * row_height + core_area.p1.y

        def get_row_idx(y: int):
            idx = int((y - core_area.p1.y) // row_height)
            return max(0, min(num_rows - 1, idx))

        def snap_to_row(position: Tuple[int, int]) -> Tuple[int, int]:
            """
            Snap to closest legal position assuming there is nothing blocked.
            :param position:
            :return:
            """
            x, y = position
            y = (y // row_height) * row_height
            y = max(min(y, max_y), min_y)

            x = max(min(x, max_x), min_x)

            return x, y

        def calculate_overlaps(positions: Dict[int, Tuple[int, int]]) -> Dict[int, int]:
            rows_lengths = np.zeros(num_rows)

            rows = [[]] * num_rows

            # Sort cells by x coordinate.
            idx_sorted = sorted(positions.keys(), key=lambda p: positions[p][0])

            for idx in idx_sorted:
                x, y = positions[idx]
                row_idx = get_row_idx(y)
                rows[row_idx].append(idx)

            overlaps = {idx: 0 for idx in positions.keys()}

            for row in rows:
                prev = None

                # For each cell check how much it overlaps with its left neighbour.
                # Sum up the overlaps.
                for cell in row:
                    if prev is not None:
                        prev_x, _ = positions[prev]

                        # Find width of previous cell.
                        shape = cell_shapes.get(circuit.subcircuit_by_id(cell).circuit_ref().name, (0, row_height))
                        prev_w, _ = shape

                        curr_x, _ = positions[cell]

                        overlap = max(0, prev_x + prev_w - curr_x)
                        # Update overlap of both cells.
                        overlaps[cell] += overlap
                        overlaps[prev] += overlap

                    prev = cell

            return overlaps

        variance_factor = 10
        for i in range(0, 1000):
            snapped_positions = {idx: snap_to_row(pos) for idx, pos in positions.items()}
            overlaps = calculate_overlaps(snapped_positions)

            for idx in positions.keys():
                x, y = positions[idx]
                overlap = overlaps[idx]
                if overlap > 0:
                    variance = (overlap / row_height * variance_factor) ** 2
                    dx = np.random.normal(0, variance)
                    dy = np.random.normal(0, variance)

                    positions[idx] = (x + dx, y + dy)

            total_overlap = sum(overlaps.values())
            if total_overlap == 0:
                break
            variance_factor *= 0.9

        positions = {idx: snap_to_row(pos) for idx, pos in positions.items()}
        return positions
