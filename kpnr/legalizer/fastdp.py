#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typing import *
from ..klayout_helper import db

from .legalizer import Legalizer
import numpy as np


class FastDPLegalizer(Legalizer):
    """
    See: http://cc.ee.ntu.edu.tw/~ywchang/Courses/PD_Source/EDA_placement.pdf (11.7.2, The FastDP algorithm)
    And: http://home.engineering.iastate.edu/~cnchu/pubs/c30.pdf (M. Pan, N. Viswanathan, and C. Chu, An efficient and effective detailed placement algorithm)
    """

    def __init__(self, row_height: int):
        super().__init__()
        assert row_height > 0
        self.row_height = row_height

    def legalize_impl(self,
                      circuit: db.Circuit,
                      core_area: db.DPolygon,
                      cell_shapes: Dict[str, Tuple[int, int]],
                      positions: Dict[int, Tuple[int, int]],
                      fixed_circuits: Optional[Set[int]] = None,
                      ) -> Dict[int, db.Trans]:
        assert isinstance(core_area, db.DBox)

        raise NotImplemented()