#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from liberty.parser import parse_liberty
from typing import Dict, List, Tuple

from ..klayout_helper import db

from .netlist.verilog_reader import VerilogNetlistReader
from .netlist.liberty_reader import LibertyNetlistReader
from .netlist.verilog_writer import VerilogNetlistWriter
from .test_data import *
from .netlist import util as net_util
import logging

logger = logging.getLogger(__name__)


def test_verilog_netlist():
    """
    1) Read a verilog netlist and a liberty library.
    2) Parse it to a db.Netlist format
    3) Write it out as verilog
    4)
    :return:
    """

    library = parse_liberty(get_example_liberty_library())

    liberty_reader = LibertyNetlistReader()

    leaf_cells = liberty_reader.read_liberty(library)

    klayout_verilog_reader = VerilogNetlistReader(leaf_circuits=leaf_cells)
    klayout_netlist = klayout_verilog_reader.read_netlist(get_example_verilog_netlist())

    top_circuit: db.Circuit = next(klayout_netlist.each_circuit_top_down())
    #print("Top circuit: {}".format(top_circuit.name))

    logger.info("Top circuit: {}".format(top_circuit.name))

    net_util.flatten(klayout_netlist)

    net_util.rename_nets_to_pin_names(klayout_netlist)
    print(klayout_netlist)

    klayout_verilog_writer = VerilogNetlistWriter()
    verilog_str = klayout_verilog_writer.write_netlist(klayout_netlist)

    return klayout_verilog_writer.write_file(os.path.join(os.path.dirname(__file__), '../test_data/seq_chip_pl.v'), verilog_str)

    #########
