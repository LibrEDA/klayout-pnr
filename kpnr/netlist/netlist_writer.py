#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from ..klayout_helper import db

class NetlistWriter:
    """
    Abstract base class for netlist writers that get the netlist in klayout.db.Netlist format.
    A netlist writers converts the klayout.db.Netlist format in a specific netlist
    format (SPICE, verilog, ...)
    """

    def write_file(self, file_path: str, netlist: str) -> int:
        """
        Read netlist from a file.
        :param file_path:
        :return:
        """
        with open(file_path, 'w') as f:
            rc = f.write(netlist)
            f.close()
            return rc

    def write_netlist(self, netlist: db.Netlist) -> str:
        """
        Read netlist from a string.
        :param data:
        :return:
        """
        raise NotImplementedError()