#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import itertools
from .netlist_reader import NetlistReader
from . import util
import networkx as nx
from verilog_parser import parser as verilog
from typing import Dict, Optional, List
from ..klayout_helper import db

import logging

logger = logging.getLogger(__name__)


class VerilogNetlistReader(NetlistReader):
    """
    Read verilog into the klayout.db netlist data structure.
    """

    def __init__(self, leaf_circuits: Optional[db.Netlist] = None):
        """

        :param leaf_circuits: Supply leaf modules that might be used in the verilog netlist but are not defined there.
            This is meant to be used to provide modules of standard cells.
        """
        self.leaf_cells = leaf_circuits

    def read_netlist(self, data: str) -> db.Netlist:
        verilog_netlist = verilog.parse_verilog(data)
        netlist = _verilog2klayoutnetlist(verilog_netlist, self.leaf_cells)

        return netlist


def _verilog2klayoutnetlist(verilog_netlist: verilog.Netlist,
                            leaf_circuits: Optional[db.Netlist] = None) -> db.Netlist:
    """
    Convert the verilog netlist data structures into a Klayout netlist.
    :param verilog_netlist:
    :param leaf_circuits: A netlist containing definitions of circuits that are referenced but not defined in `verilog_netlist`
        Meant for standard cells and macros.
    :return:
    """

    # Create container for all circuits.
    netlist = db.Netlist()

    # Tells for each module how multi-bit pins have been expanded.
    # Dict[module name, Dict[pin name, List[pin object]]]
    pin_expansion = dict()

    # If `leaf_circuits` is defined copy it to the local netlist.
    if leaf_circuits is not None:
        for circuit in leaf_circuits.each_circuit():
            netlist.add(circuit.dup())

    def create_circuit(v_module: verilog.Module) -> db.Circuit:
        """
        Process a verilog.Module and create a db.Circuit out of it.
        :param v_module:
        :return:
        """
        logger.info('Processing module: {}'.format(v_module.module_name))

        # Check that no circuit name is used more than once.
        assert netlist.circuit_by_name(v_module.module_name) is None, 'Circuit `{}` is already defined.'.format(
            v_module.module_name)

        # Find input/output and internal nets.
        wire_nets = {(decl.net_name, decl.range) for decl in v_module.net_declarations}
        input_nets = {(decl.net_name, decl.range) for decl in v_module.input_declarations}
        output_nets = {(decl.net_name, decl.range) for decl in v_module.output_declarations}

        all_net_declarations = list(itertools.chain(v_module.net_declarations,
                                                    v_module.input_declarations,
                                                    v_module.output_declarations))
        assert len(all_net_declarations) == len(set(all_net_declarations)), 'Duplicate declaration found!'

        net_declarations_by_name = {
            decl.net_name: decl for decl in all_net_declarations
        }

        logger.info("Number of nets names: {}".format(len(all_net_declarations)))

        # Get all I/O net names.
        io_net_names = set()
        io_net_names.update((n for n, _ in input_nets))
        io_net_names.update((n for n, _ in output_nets))

        wire_net_names = {n for n, _ in wire_nets}

        port_names = v_module.port_list

        logger.debug('Port names: {}'.format(port_names))

        # Sanity check: check that every port name is declared as input or output.
        ports_without_direction = set(port_names) - io_net_names
        assert not ports_without_direction, "Found ports that are neither declared as input nor output: {}".format(
            ports_without_direction)

        # Sanity check: check that every input/output declaration is actually a port name.
        unncessesary_io_declarations = io_net_names - set(port_names)
        assert not unncessesary_io_declarations, "Net declared as input/output but not in port list: {}".format(
            unncessesary_io_declarations)

        # Sanity check: All used nets should be declared.
        # Get all nets names that are connected to module instances.
        module_instance_nets = set()
        for inst in v_module.module_instances:
            for port, net in inst.ports.items():
                # print(port, '<=', net)
                if isinstance(net, str):
                    module_instance_nets.add(net)
                elif isinstance(net, verilog.Concatenation):
                    module_instance_nets.update([e if isinstance(e, str) else e.name for e in net.elements])

        undeclared_nets = module_instance_nets - (wire_net_names | io_net_names)
        assert not undeclared_nets, "Found undeclared nets: {}".format(undeclared_nets)

        # Expand multi-signal nets and create net identifiers.
        net_ids_by_name = dict()
        counter = itertools.count(start=2)  # Special nets: 0=LOW, 1=HIGH, will be connected to TIE cells.
        for decl in all_net_declarations:
            _range = decl.range
            name = decl.net_name
            if _range:
                indices = _range.to_indices()
                net_ids = [next(counter) for _ in indices]
                net_ids_by_name[name] = {idx: id for idx, id in zip(indices, net_ids)}
            else:
                net_ids_by_name[name] = next(counter)

        def find_net_ids(identifier) -> List[int]:
            """
            Given a verilog identifier (optionally indexed or sliced) find the corresponding net ID.
            :param identifier:
            :param net_mapping:
            :return:
            """
            if isinstance(identifier, verilog.Number):
                # Constant 0 has the net ID 0 and constant 1 has net ID 1.
                bits = identifier.as_bits_msb_first()
                return bits
            elif isinstance(identifier, str):
                # Only an identifier without index or slice is provided.
                # Can be a single signal or the full range of a multi-signal.
                decl = net_declarations_by_name[identifier]
                name = identifier

                assert decl.net_name == name

                ids = net_ids_by_name[name]

                if decl.range is not None:
                    # Return the full range.
                    return [ids[i] for i in decl.range.to_indices()]
                else:
                    assert isinstance(ids, int)
                    return [ids]

            elif isinstance(identifier, verilog.IdentifierIndexed):
                name = identifier.name
                indices = [identifier.index.as_integer()]
            elif isinstance(identifier, verilog.IdentifierSliced):
                name = identifier.name
                indices = identifier.range.to_indices()
            elif isinstance(identifier, verilog.NetDeclaration):
                decl = identifier
                name = identifier.net_name

                ids = net_ids_by_name[name]

                if decl.range is not None:
                    # Return the full range.
                    return [ids[i] for i in decl.range.to_indices()]
                else:
                    assert isinstance(ids, int)
                    return [ids]
            elif isinstance(identifier, verilog.Concatenation):
                ids_list = [find_net_ids(i) for i in identifier.elements]
                ids = list(itertools.chain(*ids_list))
                return ids
            else:
                assert False, 'Unsupported type for identifier: {}'.format(type(identifier))

            return [net_ids_by_name[name][i] for i in indices]

        # Resolve continuous assigns.
        # Construct a map that tells which signal is driven by which other signal.
        continuous_assign_map = dict()
        for assign in v_module.assignments:
            assert isinstance(assign, verilog.ContinuousAssign)
            for left, right in assign.assignments:
                if not isinstance(left, verilog.Concatenation):
                    left = verilog.Concatenation([left])
                if not isinstance(right, verilog.Concatenation):
                    right = verilog.Concatenation([right])
                # print('assignment', left, '<=', right)

                left_ids = [find_net_ids(i) for i in left.elements]
                left_ids = list(itertools.chain(*left_ids))

                right_ids = [find_net_ids(i) for i in right.elements]
                right_ids = list(itertools.chain(*right_ids))

                # print('assignment', left_ids, '<=', right_ids)

                assert len(left_ids) == len(right_ids), \
                    "Mismatch of bit-length in assignment: '{} = {}', {} != {}".format(left, right, len(left_ids),
                                                                                       len(right_ids))

                for l, r in zip(left_ids, right_ids):
                    continuous_assign_map[l] = r

        logger.info("Number of expanded nets names: {}".format(
            counter
        ))

        # Create a mapping from net ids to expanded net name.
        expanded_net_names = dict()
        for net_decl in all_net_declarations:
            for id in find_net_ids(net_decl):
                expanded_net_names[id] = '{}_{}'.format(net_decl.net_name, id)

        # Create parent circuit.
        parent_circuit = db.Circuit()
        parent_circuit.name = v_module.module_name

        netlist.add(parent_circuit)

        # Construct module ports.
        _pin_expansion = dict()  # Remember how multi-signal pins have been expanded
        for net_decl in v_module.input_declarations:  # TODO FIXME: Preserve the ordering of the ports.
            for id in find_net_ids(net_decl):
                net_name = expanded_net_names[id]
                # Store I/O direction (input).
                pin = parent_circuit.create_pin(net_name) # TODO: Due to a bug in klayout `pin` cannot directly be used to set property
                pin_obj = parent_circuit.pin_by_name(net_name)
                util.set_pin_direction(pin_obj, 'input')

                _pin_expansion.setdefault(net_decl.net_name, []).append(pin)
                assert util.is_input_pin(pin_obj)

        for net_decl in v_module.output_declarations:
            for id in find_net_ids(net_decl):
                net_name = expanded_net_names[id]
                # Store direction information (output).
                # For now the direction is stored by appending '_o' to the pin name.
                pin = parent_circuit.create_pin(net_name) # TODO: Due to a bug in klayout `pin` cannot directly be used to set property
                pin_obj = parent_circuit.pin_by_name(net_name)
                util.set_pin_direction(pin_obj, 'output')

                _pin_expansion.setdefault(net_decl.net_name, []).append(pin)
                assert util.is_output_pin(pin_obj)

        pin_expansion[v_module.module_name] = _pin_expansion

        # Create Net objects that reside in the namespace of the parent module.
        nets_by_id = {
            # Logic LOW and HIGH have a dedicated net.
            0: parent_circuit.create_net('__LOW__'),
            1: parent_circuit.create_net('__HIGH__'),
        }
        for id, net_name in expanded_net_names.items():
            net = parent_circuit.create_net(net_name)
            nets_by_id[id] = net

        # Make internal connections to pins.
        # Loop over pins and attach them to the nets.
        for pin_name in v_module.port_list:
            # Find net IDs of this pin.
            net_ids = find_net_ids(pin_name)

            expanded_pins = pin_expansion.get(v_module.module_name, dict()).get(pin_name, None)

            if expanded_pins is not None:
                assert len(net_ids) == len(expanded_pins), "Length does not match: {} != {}" \
                    .format(len(net_ids), len(expanded_pins))

                nets = [nets_by_id[id] for id in net_ids]
                for pin, net in zip(expanded_pins, nets):
                    assert isinstance(pin, db.Pin)
                    parent_circuit.connect_pin(pin, net)
            else:
                assert len(net_ids) == 1, "Arrays not supported yet."
                net = nets_by_id[net_ids[0]]
                pin = parent_circuit.pin_by_name(pin_name)
                parent_circuit.connect_pin(pin, net)

        # Make module instances and connections to them.
        for v_inst in v_module.module_instances:
            # Find module by name.
            leaf_circuit: db.Circuit = netlist.circuit_by_name(v_inst.module_name)

            assert leaf_circuit is not None, "Module '{}' can't be found.".format(v_inst.module_name)

            # Create sub-circuit instance.
            inst: db.SubCircuit = parent_circuit.create_subcircuit(leaf_circuit, v_inst.instance_name)
            assert isinstance(inst, db.SubCircuit)

            # Loop over pins and attach them to the nets.
            for pin_name, net_identifier in v_inst.ports.items():
                net_ids = find_net_ids(net_identifier)

                circuit_name = leaf_circuit.name
                assert v_module.module_name != circuit_name

                expanded_pins = pin_expansion.get(circuit_name, dict()).get(pin_name, None)

                if expanded_pins is not None:
                    assert len(net_ids) == len(expanded_pins), "Length does not match: {} != {}" \
                        .format(len(net_ids), len(expanded_pins))

                    nets = [nets_by_id[id] for id in net_ids]
                    for pin, net in zip(expanded_pins, nets):
                        inst.connect_pin(pin, net)
                else:
                    assert len(net_ids) == 1, "Arrays not supported yet."
                    net = nets_by_id[net_ids[0]]
                    pin = leaf_circuit.pin_by_name(pin_name)
                    inst.connect_pin(pin, net)

        # Process continuous assign statements.
        # Replace all left-hand side nets by the right-hand side nets.
        for left_id, right_id in continuous_assign_map.items():
            # print('assign', left_id, '=', right_id)
            old_net = nets_by_id[left_id]
            new_net = nets_by_id[right_id]

            util.rename_net(parent_circuit, old_net, new_net)

        return parent_circuit

    # Create dependency graph of modules.
    dependency_graph = nx.DiGraph()

    for v_module in verilog_netlist.modules:
        deps = {inst.module_name for inst in v_module.module_instances}

        for dep in deps:
            dependency_graph.add_edge(v_module.module_name, dep)

    cycles = list(nx.simple_cycles(dependency_graph))
    assert len(cycles) == 0, "Circular dependencies found: {}".format(cycles)

    # Find order to load the modules such that dependencies are always already processed.
    order = list(nx.dfs_postorder_nodes(dependency_graph))

    # Find modules that are not yet loaded.
    order = [e for e in order if netlist.circuit_by_name(e) is None]

    v_modules_by_name = {
        m.module_name: m for m in verilog_netlist.modules
    }

    # Add all circuits to a netlist.
    for name in order:
        create_circuit(v_modules_by_name[name])

    # Delete unused nets.
    netlist.purge_nets()

    return netlist
