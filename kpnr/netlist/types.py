#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import itertools
import logging
from typing import List, Dict, Set, Optional, Iterable, Iterator, Any
from enum import Enum
from collections import Counter

logger = logging.getLogger(__name__)


# class Direction(Enum):
#     """
#     Direction of pins.
#     """
#     Input = 0
#     Output = 1
#     InOut = 2
#     Clock = 3
#     Supply = 4
#     Ground = 5
#
#     def is_input(self):
#         return self.value == Direction.Input
#
#     def is_output(self):
#         return self.value == Direction.Output
#
#     @staticmethod
#     def from_string(s: str):
#         """
#         Create `Direction` object based on a name. Matching is case insensitive.
#         :return:
#         """
#         direction_map = {
#             'input': Direction.Input,
#             'output': Direction.Output,
#             'inout': Direction.InOut,
#             'clock': Direction.Clock,
#             'supply': Direction.Supply,
#             'ground': Direction.Ground
#         }
#         return direction_map[s.lower()]

# This is the old netlist data structure which has been replaced by klayout.db.Netlist. RIP.
# class Net:
#
#     def __init__(self, name):
#         self.name = name
#
#     def __repr__(self):
#         return "Net({})".format(self.name)
#
#     def __str__(self):
#         return self.__repr__()
#
#     def format(self):
#         return str(self)
#
#
# class Range:
#
#     def __init__(self, start, end):
#         self.start = start
#         self.end = end
#
#     def to_indices(self):
#         """
#         Convert to list of indices in the range.
#         :return:
#         """
#         return list(reversed(range(self.end.as_integer(), self.start.as_integer() + 1)))
#
#     def __repr__(self):
#         return "[{}:{}]".format(self.start, self.end)
#
#
# class NetVector:
#
#     def __init__(self, name: str, range: Range):
#         self.name = name
#         self.range = range
#
#     def __repr__(self):
#         return "{}{}".format(self.name, self.range)
#
#
# class PinDefinition:
#
#     def __init__(self, name, direction: Direction):
#         # Name of the pin
#         self.name = name
#         # Direction: input or output
#         self.direction = direction
#         # Net that is connected to this pin from within the module.
#         self.internal_net = Net('{}'.format(name))
#
#     def __repr__(self):
#         return "{}({})".format(self.direction.name, self.name)
#
#
# class PinInstance:
#
#     def __init__(self, pin: PinDefinition, module_instance):
#         self.module_instance = module_instance
#         self.pin = pin
#         self.net = None
#
#     def __repr__(self):
#         return "PinInstance({}, {})".format(self.pin, self.net)
#
#     def disconnect(self):
#         assert self.module_instance.parent_module is not None, "Module instance of this pin has no parent module. ({}, {})" \
#             .format(self.module_instance, self)
#         self.module_instance.parent_module.disconnect_pin(self)
#
#     def connect(self, net: Net):
#         assert self.module_instance.parent_module is not None, "Module instance of this pin has no parent module. ({}, {})" \
#             .format(self.module_instance, self)
#         self.module_instance.parent_module.connect_pin(self, net)
#
#
# def format(x, indent=0, indent_char='  '):
#     if isinstance(x, str) or x is None:
#         return indent_char * indent + str(x)
#     elif isinstance(x, list):
#         return "\n".join((format(e, indent=indent + 1, indent_char=indent_char) for e in x))
#     else:
#         return format(x.format(), indent=indent, indent_char=indent_char)
#
#
# class Module:
#     """
#     Description of a module (device) that can be used to create module instances.
#     """
#
#     def __init__(self, name: str, pin_definitions: List[PinDefinition], **user_parameters):
#         """
#
#         :param name:
#         :param pin_definitions:
#         :param virtual: Tells if this does not correspond to a physical module. Can be used as a center node when converting
#             multi-terminal nets into a star network.
#         """
#         self.name = name
#         self.pin_definitions = pin_definitions
#         # self._modules = set()
#         self._module_instances = set()
#         self._nets = set()
#         self.user_parameters = user_parameters
#         self._pin_instances_by_net = dict()
#
#         for p in pin_definitions:
#             p.internal_net = self.create_net(p.name)
#
#     def format(self) -> List:
#         l = []
#         l.append("{} ({}) {{".format(self.name, ", ".join((p.name for p in self.pin_definitions))))
#         l.append(
#             [n.format() for n in self._nets]
#         )
#         l.extend(
#             [m.format() for m in self._module_instances]
#         )
#         l.append("}")
#         return l
#
#     def __repr__(self):
#         return "Module({}, {})".format(self.name, self.pin_definitions)
#
#     def pin_definition_by_index(self, index: int) -> PinDefinition:
#         return self.pin_definitions[index]
#
#     def pin_definition_by_name(self, pin_name) -> PinDefinition:
#         for i, p in enumerate(self.pin_definitions):
#             if p.name == pin_name:
#                 return p
#         assert False, "No such pin: {}".format(pin_name)
#
#     def create_net(self, name) -> Net:
#         net = Net(name)
#         self._nets.add(net)
#         return net
#
#     # def register_module(self, module: Module) -> None:
#     #     assert isinstance(module, Module)
#     #     self._modules.add(module)
#
#     def _assert_net_is_registered(self, net: Net):
#         assert net in self._nets, "Net '{}' is not registered. Register net before.".format(net)
#
#     def add_module_instance(self, module_instance) -> None:
#         assert isinstance(module_instance, ModuleInstance)
#         assert module_instance not in self._module_instances, "Module instance already exists."
#         # assert module_instance.module in self._modules, "Module must be registered before."
#         for pin in module_instance.pin_instances:
#             if pin.net is not None:
#                 assert pin.net in self._nets, "Net '{}' on pin '{}' is not registered. Register net before.".format(
#                     pin.net,
#                     pin)
#         module_instance.parent_module = self
#         self._module_instances.add(module_instance)
#
#         # Link module instance such that it can be found by the nets it is connected to.
#         for pin in module_instance.pin_instances:
#             self._pin_instances_by_net.setdefault(pin.net, set()).add(pin)
#
#     def remove_module_instance(self, module_instance) -> None:
#         assert isinstance(module_instance, ModuleInstance)
#         assert module_instance in self._module_instances, "Cannot remove because ModuleInstance is not registered."
#
#         # Remove link from net-to-instance mapping.
#         for pin_inst in module_instance.pin_instances:
#             self.disconnect_pin(pin_inst)
#
#         module_instance.parent_module = None
#         self._module_instances.remove(module_instance)
#
#     def disconnect_pin(self, pin_inst: PinInstance) -> None:
#         assert isinstance(pin_inst, PinInstance)
#         assert pin_inst.module_instance in self._module_instances, \
#             "ModuleInstance '{}' is not registered.".format(pin_inst.module_instance)
#
#         if pin_inst.net is not None:
#             net = pin_inst.net
#             pin_set = self._pin_instances_by_net.get(net, set())
#             pin_set.remove(pin_inst)
#
#             pin_inst.net = None
#
#     def connect_pin(self, pin_inst: PinInstance, net: Net) -> None:
#         """
#         Connect a pin of a child module instance to a net.
#         :param pin_inst:
#         :param net:
#         """
#         assert isinstance(pin_inst, PinInstance)
#         assert pin_inst.module_instance in self._module_instances, \
#             "ModuleInstance '{}' is not registered.".format(pin_inst.module_instance)
#
#         self._assert_net_is_registered(net)
#
#         # Disconnect first if necessary.
#         self.disconnect_pin(pin_inst)
#
#         if net is not None:
#             pin_inst.net = net
#             self._pin_instances_by_net.setdefault(net, set()).add(pin_inst)
#
#     def find_pin_instances_by_net(self, net: Net) -> Set[PinInstance]:
#         """
#         Find all pin instances that are connected to `net`.
#         :param net:
#         :return:
#         """
#         return self._pin_instances_by_net.get(net, set())
#
#     def find_module_instances_by_net(self, net: Net) -> Set:
#         """
#         Find all module instances that are connected to `net`.
#         :param net:
#         :return:
#         """
#         pins = self.find_pin_instances_by_net(net)
#
#         return {pin.module_instance for pin in pins}
#
#     def module_instances_by_net(self) -> Dict[Net, Any]:
#         modules_by_net = dict()
#         for net, pins in self._pin_instances_by_net.items():
#             for pin in pins:
#                 modules_by_net.setdefault(net, set()).add(pin.module_instance)
#         return modules_by_net
#
#     def replace_net(self, old_net: Net, new_net: Net):
#         """
#         Disconnect all terminals of `old_net` and connect them to `new_net`.
#         :param old_net:
#         :param new_net:
#         """
#         self._assert_net_is_registered(old_net)
#         self._assert_net_is_registered(new_net)
#
#         for pin in self.find_pin_instances_by_net(old_net).copy():
#             pin.connect(new_net)
#
#     def each_net(self) -> Iterator[Net]:
#         return iter(self._nets)
#
#     def each_instance(self) -> Iterator:
#         return iter(self._module_instances)
#
#     def flatten(self):
#         flat = Module(name=self.name, pin_definitions=self.pin_definitions)
#
#
# class ModuleInstance:
#
#     def __init__(self, module: Module, id):
#         self.module = module
#         self.parent_module = None
#         self.id = id
#
#         self.pin_instances = [PinInstance(pin, self) for pin in module.pin_definitions]
#
#     def format(self):
#         return [
#             "{} {} ({});".format(self.module.name, self.id,
#                                  ", ".join(("{} <= {}".format(p.pin.name, p.net) for p in self.pin_instances)))
#         ]
#
#     def pin_by_id(self, index: int) -> PinInstance:
#         return self.pin_instances[index]
#
#     def pin_by_def(self, pin_definition: PinDefinition) -> PinInstance:
#         matches = [p for p in self.pin_instances if p.pin == pin_definition]
#         assert len(matches) > 0, "So such PinDefinition: {}".format(pin_definition)
#         return matches[0]
#
#     def pin_by_name(self, pin_name) -> PinInstance:
#         for i, p in enumerate(self.pin_instances):
#             if p.pin.name == pin_name:
#                 return p
#         assert False, "No such pin: {}".format(pin_name)
#
#     def each_pin(self) -> Iterator[PinInstance]:
#         return iter(self.pin_instances)
#
#     def _get_nets_by_direction(self, direction: Direction) -> List[Net]:
#         assert isinstance(direction, Direction)
#         return [pin.net for pin in self.pin_instances if pin.pin.direction == direction]
#
#     def input_nets(self) -> List[Net]:
#         """
#         Return a list of nets that are connected to a input pin of this module.
#         """
#         return self._get_nets_by_direction(Direction.Input)
#
#     def output_nets(self) -> List[Net]:
#         """
#         Return a list of nets that are connected to a output pin of this module.
#         """
#         return self._get_nets_by_direction(Direction.Output)
#
#     def __repr__(self):
#         pin_connections = ", ".join(['{}={}'.format(p.pin, p.net) for p in self.pin_instances])
#         return "ModuleInstance({}, {}, {})".format(self.module.name, self.id, pin_connections)
#
#     def __lt__(self, other):
#         assert isinstance(other, ModuleInstance), "Cannot compare {} with {}".format(type(self), type(other))
#         return self.id < other.id
#
#
# # class FlatNetlist:
# #
# #     def __init__(self):
# #         self._modules = set()
# #         self._module_instances = set()
# #         self._nets = set()
# #         # self._net_id_counter = itertools.count(start=2)
# #         self._module_instances_by_net = dict()
# #
# #     def register_net(self, net: Net) -> None:
# #         assert isinstance(net, Net)
# #         self._nets.add(Net)
# #
# #     def register_module(self, module: Module) -> None:
# #         assert isinstance(module, Module)
# #         self._modules.add(module)
# #
# #     def add_module_instance(self, module_instance: ModuleInstance) -> None:
# #         assert isinstance(module_instance, ModuleInstance)
# #         assert module_instance.module in self._modules, "Module must be registered before."
# #         for pin, net in module_instance.port_mapping().items():
# #             assert net in self._nets, "Net '{}' on pin '{}' is not registered. Register net before.".format(net, pin)
# #         self._module_instances.add(module_instance)
# #
# #         # Link module instance such that it can be found by the nets it is connected to.
# #         for pin, net in module_instance.port_mapping().items():
# #             self._module_instances_by_net.setdefault(net, set()).add(module_instance)
# #
# #     def remove_module_instance(self, module_instance: ModuleInstance) -> None:
# #         assert isinstance(module_instance, ModuleInstance)
# #         assert module_instance in self._module_instances, "Cannot remove because ModuleInstance is not registered."
# #
# #         # Remove link from net-to-instance mapping.
# #         for pin, net in module_instance.port_mapping().items():
# #             inst_set = self._module_instances_by_net[net]
# #             assert module_instance in inst_set
# #             inst_set.remove(module_instance)
# #
# #     def find_instances_by_net(self, net: Net) -> Set[ModuleInstance]:
# #         """
# #         Find all module instances that are connected to `net`.
# #         :param net:
# #         :return:
# #         """
# #         return self._module_instances_by_net.get(net, default=set())
#
#
# def test_netlist():
#     top = Module(name='my_top_module',
#                  pin_definitions=[PinDefinition('A', Direction.Input), PinDefinition('Z', Direction.Output)])
#
#     sub = Module(name='my_sub_module',
#                  pin_definitions=[PinDefinition('A', Direction.Input), PinDefinition('B', Direction.Input),
#                                   PinDefinition('Z', Direction.Output)])
#
#     sub_inst1 = ModuleInstance(sub, id='sub_1')
#
#     assert sub_inst1 in {sub_inst1}
#
#     top.add_module_instance(sub_inst1)
#
#     sub_inst1.pin_by_id(0).connect(top.pin_definition_by_index(0).internal_net)
#
#     top.connect_pin(sub_inst1.pin_by_id(1), top.create_net('newnet'))
#     assert sub_inst1.pin_by_id(0).net is not None
#
#     top.replace_net(sub_inst1.pin_by_id(0).net, top.create_net('replacement'))
#
#     print(top)
#     print(set(top.each_net()))
#     print(set(sub.each_net()))
#
#     print(sub_inst1)
#
#     for n in top.each_net():
#         print(n)
#         print(top.find_module_instances_by_net(n))
#     print()
#     print(format(top))
