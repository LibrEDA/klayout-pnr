#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from ..klayout_helper import db
from .netlist_writer import NetlistWriter
from . import util
import sys
import logging

logger = logging.getLogger(__name__)


class VerilogNetlistWriter(NetlistWriter):
    """
    Write verilog from the klayout.db netlist data structure.
    """

    def write_netlist(self, netlist: db.Netlist) -> str:
        verilog = _klayoutnetlist2verilog(netlist)

        return verilog


def _klayoutnetlist2verilog(netlist: db.Netlist) -> str:
    # iterate over all circuits in netlist (=modules)
    top_circuit = list(netlist.each_circuit_top_down())[0]

    # we only expect circuits
    assert (isinstance(top_circuit, db.Circuit))

    # extract the pin list
    pins = list(top_circuit.each_pin())

    # extract net names
    nets = list(top_circuit.each_net())

    # extract submodules
    submodules = []
    for submodule in list(top_circuit.each_subcircuit()):
        submodule_ele = []
        assert (isinstance(submodule, db.SubCircuit))
        submodule_ref = submodule.circuit_ref()
        submodule_ele.append(submodule_ref.name)
        submodule_ele.append(submodule.name)
        for pin in list(submodule_ref.each_pin()):
            pin_net_name = submodule.net_for_pin(pin.id()).name

            submodule_ele.append([pin.name(), pin_net_name])
        submodules.append(submodule_ele)

    return verilog_pp(top_circuit, pins, nets, submodules)


def verilog_pp(top_circuit: db.Circuit, pins: list, nets: list, submodules: list) -> str:
    res = '// This file was automatically generated by verilog_writer\n\n'
    res += '''module {} (\n'''.format(top_circuit.name)

    # port map
    pin_nets = []  # used to keep track which nets are pins
    for pin in pins:
        pin = pin.name()
        res += '\t' + pin + ',\n'
        pin_nets.append(pin)
    res = res + '\n);\n\n'

    # pin direction
    for pin in pins:
        pin_direction = util.get_pin_direction(pin)
        res += '\t' + pin_direction + ' ' + pin.name() + ';\n'
    res += '\n'

    # write wire declaration for pins
    for pin in pins:
        res += '\twire ' + pin.name() + ';\n'
    res += '\n'

    # write internal nets
    for net in nets:
        net = net.name
        if net not in pin_nets:
            res += '\twire ' + net.replace('.', '_') + ';\n'
    res += '\n'

    # find mismatches in pin and net name for the top module, use assign to fix it
    for pin in pins:
        pin_net_name = top_circuit.net_for_pin(pin.id()).name

        # found a mismatch -> connect them by assign statements
        if pin_net_name not in pin_nets:

            # check the assign direction
            if util.is_output_pin(pin):
                res += '\tassign ' + pin.name() + ' = ' + pin_net_name + ';\n'
            elif util.is_input_pin(pin):
                res += '\tassign ' + pin_net_name + ' = ' + pin.name() + ';\n'
            else:
                assert False, 'Cannot assign to pin with direction `{}`. Netlist not usable.'.format(
                    util.get_pin_direction(pin))
    res += '\n'

    # module instances
    for instance in submodules:
        inst_len = len(instance)
        res += '\t' + instance[0] + ' ' + instance[1].replace('.', '_') + '(\n'
        for i in range(2, inst_len):
            port_mapping = instance[i]
            res += '\t\t.{}( {} ),\n'.format(port_mapping[0].replace('.', '_'), port_mapping[1].replace('.', '_'))
        res = res + '\n\t);\n\n'

    # endmodule
    res += 'endmodule : {}'.format(top_circuit.name)

    return res
