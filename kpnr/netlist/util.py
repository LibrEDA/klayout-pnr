#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from ..klayout_helper import db
from typing import Set, Tuple

"""
Collection of netlist manipulation functions.
"""


def set_pin_direction(pin: db.Pin, direction: str) -> None:
    """
    Write a string representing the pin direction into the properties of the pin.
    The property key is 'direction'.
    :param pin:
    :param direction: One of {'input', 'output'}
    """
    assert direction in {'input', 'output'}
    pin.set_property('direction', direction)


def get_pin_direction(pin: db.Pin) -> str:
    return pin.property('direction')


def is_input_pin(pin: db.Pin) -> bool:
    """
    Check if this pin is marked as an input pin.
    :param pin:
    :return:
    """
    return get_pin_direction(pin) == 'input'


def is_output_pin(pin: db.Pin) -> bool:
    """
    Check if this pin is marked as an output pin.
    :param pin:
    :return:
    """
    return get_pin_direction(pin) == 'output'


def get_input_pins(subcircuit: db.SubCircuit) -> Set[db.Pin]:
    return {p for p in subcircuit.circuit_ref().each_pin() if is_input_pin(p)}


def get_output_pins(subcircuit: db.SubCircuit) -> Set[db.Pin]:
    return {p for p in subcircuit.circuit_ref().each_pin() if is_output_pin(p)}


def neighbours(subcircuit: db.SubCircuit, output_pin: db.Pin) -> Set[Tuple[db.SubCircuit, db.Pin]]:
    """
    Find all subcircuits that are connected to `output_pin` of `subcircuit`.
    :param subcircuit:
    :param output_pin:
    :return:
    """
    result = set()

    net = subcircuit.net_for_pin(output_pin.id())

    # Find connections to other subcircuits.
    for pin_ref in net.each_subcircuit_pin():
        pin = pin_ref.pin()
        other_subcircuit = pin_ref.subcircuit()
        # Find all other pins connected to this net.
        if other_subcircuit.id() != subcircuit.id():
            result.add((other_subcircuit, pin))

    return result


def rename_nets_to_pin_names(netlist: db.Netlist) -> None:
    """
    Make sure that all nets connecting to a pin of a circuit have the same name as this pin.
    :param netlist:
    :return:
    """
    for circuit in netlist.each_circuit():
        for pin in circuit.each_pin():
            net = circuit.net_for_pin(pin.id())
            if net is not None:
                net.name = pin.name()


def flatten(netlist: db.Netlist) -> None:
    """
    Flatten netlist without removing empty leaf cells.
    This is used to flatten a netlist which includes empty circuits that represent standard-cells.
    :param netlist:
    :return:
    """
    flattening_order = []

    for circuit in netlist.each_circuit_bottom_up():
        num_subcircuits = len(list(circuit.each_subcircuit()))
        if num_subcircuits > 0:
            flattening_order.append(circuit)

    non_leaf_cells = set(flattening_order)

    for circuit in flattening_order:
        for sub in circuit.each_subcircuit():
            assert isinstance(sub, db.SubCircuit)

            if sub.circuit_ref() in non_leaf_cells:
                circuit.flatten_subcircuit(sub)


def rename_net(circuit: db.Circuit, old_net: db.Net, new_net: db.Net) -> None:
    """
    Replace a net `old_net` by `new_net` within the `circuit`.
    :param circuit:
    :param old_net:
    :param new_net:
    """
    assert isinstance(old_net, db.Net)
    assert isinstance(new_net, db.Net)

    # Handle outgoing pins.
    for pin_ref in list(old_net.each_pin()):
        pin = pin_ref.pin()
        circuit.connect_pin(pin, new_net)

    # Handle connections to sub-circuits.
    for pin_ref in list(old_net.each_subcircuit_pin()):
        pin = pin_ref.pin()
        assert isinstance(pin, db.Pin)
        # Find subcircuit.
        subcircuit = circuit.subcircuit_by_id(pin_ref.subcircuit().id())
        # Change pin connection to righ-hand side net.
        subcircuit.connect_pin(pin, new_net)


def replace_subcircuit(original: db.SubCircuit,
                       replacement: db.Circuit,
                       match_pin_names: bool = False) -> None:
    """
    Replace a subcircuit by another one.
    Pins are connected by order or by name if `match_pin_names` is set to True.
    :param original:
    :param replacement:
    :param match_pin_names: Tell if pins should be connected according to their names instead of position.
    """
    original_pins = list(original.circuit_ref().each_pin())
    new_pins = list(replacement.each_pin())

    assert len(original_pins) == len(new_pins), \
        "`{}` and `{}` circuit don't have the same number of pins.".format(original.circuit_ref().name,
                                                                           replacement.name)

    if match_pin_names:
        original_pin_names = {p.name() for p in original_pins}
        new_pin_names = {p.name() for p in new_pins}
        assert original_pin_names == new_pin_names, 'Replacement circuit must have the same pin names as the original circuit.'

    parent_circuit: db.Circuit = original.circuit()

    # Create new subcircuit instance.
    new_sub = parent_circuit.create_subcircuit(replacement, original.name)

    # Attach nets.
    for pin in original_pins:
        net = original.net_for_pin(pin.id())
        if match_pin_names:
            new_sub.connect_pin(replacement.pin_by_name(pin.name()), net)
        else:
            new_sub.connect_pin(pin, net)

    # Remove the old circuit.
    parent_circuit.remove_subcircuit(original)


def test_replace_subcircuit():
    """
    Replace a subcircuit by another one.
    :return:
    """
    netlist = db.Netlist()
    c1 = db.Circuit()
    c1.name = 'C1'
    c1.create_pin('A')
    c1.create_pin('B')
    netlist.add(c1)

    c2 = db.Circuit()
    c2.name = 'C2'
    c2.create_pin('B')
    c2.create_pin('A')
    netlist.add(c2)

    top = db.Circuit()
    top.name = 'TOP'
    netlist.add(top)
    s1 = top.create_subcircuit(c1, 'subcircuit_i')
    net1 = top.create_net('net1')
    net2 = top.create_net('net2')
    s1.connect_pin(c1.pin_by_id(0), net1)

    print(netlist)
    replace_subcircuit(s1, c2, match_pin_names=True)
    print(netlist)

    assert len(list(top.each_subcircuit())) == 1
    assert top.subcircuit_by_name('subcircuit_i').name == 'subcircuit_i'


def insert_subcircuit(right_pins: Set[Tuple[db.SubCircuit, db.Pin]],
                      circuit: db.Circuit,
                      left_pin: db.Pin,
                      right_pin: db.Pin,
                      circuit_name='__inserted_circuit__',
                      net_name='__inserted_net__'
                      ) -> db.SubCircuit:
    """
    Insert a subcircuit in front of a set of other subcircuit pins by splitting the net.

    {some pins}---<net>-----------------------------------------{pins_after, ...}
    {some pins}---<net>---{pin_before pin_after}---<new net>---{pins_after, ...}

    :param right_pins: Set of (subcircuit, pin) that tells which subcircuit pins should be attached after the inserted subcircuit.
    :param circuit: Circuit to be inserted.
    :param left_pin: Pin of the inserted subcircuit that is connected to the original net.
    :param right_pin: Pin of the inserted subcircuit that is connected to 'pins_after' by the generated net.
    :param circuit_name: Name of inserted subcircuit.
    :param net_name: Name of generated net.
    :returns: Returns the created subcircuit.
    """

    assert len(right_pins) > 0, 'Require '

    nets = {subcircuit.net_for_pin(pin.id()) for subcircuit, pin in right_pins}
    assert len(nets) == 1, "Pins `pins_after` must all be connected to the same net."
    net = nets.pop()

    # Get parent circuit.
    parent_circuit = {subcircuit.circuit() for subcircuit, pin in right_pins}
    assert len(parent_circuit) == 1, 'Must have exactly one parent circuit!'
    parent_circuit = parent_circuit.pop()

    # Create instance of inserted circuit.
    inst = parent_circuit.create_subcircuit(circuit, circuit_name)

    # Create net that results from splitting the available net.
    net_after = parent_circuit.create_net(net_name)

    # Connect inserted circuit.
    inst.connect_pin(right_pin, net_after)
    inst.connect_pin(left_pin, net)

    for subcircuit, pin in right_pins:
        subcircuit.connect_pin(pin, net_after)

    return inst


def remove_subcircuit(subcircuit: db.SubCircuit) -> None:
    """
    Remove a subcircuit and connect the all adjacent nets together.
    :param subcircuit:
    :return:
    """

    parent = subcircuit.circuit()
    pins = list(subcircuit.circuit_ref().each_pin())
    nets = [subcircuit.net_for_pin(p.id()) for p in pins]

    # Take the net with the shortest name as a replacement.
    replacement_net = min(nets, key=lambda net: len(net.name))

    parent.remove_subcircuit(subcircuit)

    for n in nets:
        if n != replacement_net:
            rename_net(parent, n, replacement_net)


def test_insert_subcircuit():
    netlist = db.Netlist()
    c1 = db.Circuit()
    c1.name = 'SUB'
    c1.create_pin('A')
    c1.create_pin('B')
    netlist.add(c1)

    buf = db.Circuit()
    buf.name = 'BUF'
    buf.create_pin('IN')
    buf.create_pin('OUT')
    netlist.add(buf)

    top = db.Circuit()
    top.name = 'TOP'
    netlist.add(top)
    s1 = top.create_subcircuit(c1, 'sub_1')
    s2 = top.create_subcircuit(c1, 'sub_2')
    net1 = top.create_net('net1')

    s1.connect_pin(c1.pin_by_id(1), net1)
    s2.connect_pin(c1.pin_by_id(0), net1)

    print(netlist)
    buf_inst = insert_subcircuit(
        {(s2, c1.pin_by_name('A'))},
        buf,
        buf.pin_by_name('IN'),
        buf.pin_by_name('OUT'),
        circuit_name='mybuf'
    )

    rename_nets_to_pin_names(netlist)
    print(netlist)

    remove_subcircuit(buf_inst)
    print(netlist)
