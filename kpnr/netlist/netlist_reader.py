#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from ..klayout_helper import db


class NetlistReader:
    """
    Abstract base class for netlist readers that output the netlist in klayout.db.Netlist format.
    A netlist reader converts a specific netlist format (SPICE, verilog, ...) into the
    netlist data structure defined in this package.

    The KLayout netlist does not support (yet) tha annotation of I/O directions for pins. Therefore,
    `properties` are used to store this information.

    * db.Pin.property('direction'): is assigned 'input' for input pins, 'output'' for output pins.
    """

    def read_file(self, file_path: str) -> db.Netlist:
        """
        Read netlist from a file.
        :param file_path:
        :return:
        """
        with open(file_path, 'r') as f:
            data = f.read()
            return self.read_netlist(data)

    def read_netlist(self, data: str) -> db.Netlist:
        """
        Read netlist from a string.
        :param data:
        :return:
        """
        raise NotImplementedError()
