#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from .netlist_reader import NetlistReader
from liberty.parser import parse_liberty
import liberty.types
from . import util
from ..klayout_helper import db
import logging

logger = logging.getLogger(__name__)


class LibertyNetlistReader(NetlistReader):
    """
    Read verilog into the klayout.db netlist data structure.
    """

    def read_liberty(self, library: liberty.types.Group) -> db.Netlist:
        return _create_klayout_leaf_cells_from_liberty(library)

    def read_netlist(self, data: str) -> db.Netlist:
        library = parse_liberty(data)
        return self.read_liberty(library)


def _create_klayout_leaf_cells_from_liberty(liberty_library: liberty.types.Group) -> db.Netlist:
    """
    Create a netlist populated with `Circuit` data structures of the cells defined in the liberty library.
    The `Circuit`s themselves remain empty.
    :param liberty_library:
    :return: db.Netlist
    """

    netlist = db.Netlist()

    # Find all cells defined in the library and create modules.
    for cell in liberty_library.get_groups('cell'):
        cell_name = cell.args[0]
        circuit = db.Circuit()
        circuit.name = cell_name

        for pin in cell.get_groups('pin'):
            pin_name = pin.args[0]
            direction_name = pin.get('direction')

            circuit.create_pin(pin_name)

            # Store pin I/O direction.
            pin_obj = circuit.pin_by_name(pin_name)  # TODO: Due to a bug in klayout `pin` cannot directly be used to set property.
            util.set_pin_direction(pin_obj, direction_name)

        netlist.add(circuit)

    return netlist
