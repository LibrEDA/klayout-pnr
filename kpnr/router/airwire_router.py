#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from .router import *
import logging
from ..layout import polygon_label
import itertools
import collections
from typing import Tuple

logger = logging.getLogger(__name__)


class AirwireRouter(Router):
    """
    Dummy router. Visualizes the connections with direct lines ('air wires').
    """

    def __init__(self, airwire_layer: Tuple[int, int]):
        """
        Parameters
        ----------
        airwire_layer: Layer on which the air wires shall be placed.
        """
        self.airwire_layer = airwire_layer

    def route_impl(self,
                   netlist: db.Circuit,
                   layout: db.Layout,
                   core_area: db.DSimplePolygon,
                   top_cell: db.Cell,
                   routing_terminals: db.Cell):

        # Extract routing terminals of each net.
        logger.debug("Extract routing terminals of each net.")
        pin_shapes_by_net = collections.defaultdict(set)

        # Iterate over each layer.
        for layer_id in layout.layer_indices():
            routing_terminal_shapes: db.Shapes = routing_terminals.shapes(layer_id)
            for s in (s for s in routing_terminal_shapes.each() if not s.is_text()):
                assert isinstance(s, db.Shape)

                net_name = s.property('net_name')
                #terminal_id = s.property('terminal_id')

                pin_shapes_by_net[net_name].add(s)

        # === Draw air wires ===

        logger.info("Draw air-wires.")

        airwire_layer = layout.layer(db.LayerInfo(*self.airwire_layer, 'airwire_routes'))
        airwire_cell = layout.create_cell("airwire_routes") # Create a cell holding the routes.
        airwire_shapes = airwire_cell.shapes(airwire_layer)
        for net_name, pin_shapes in pin_shapes_by_net.items():
            for a, b in itertools.combinations(pin_shapes, 2):

                pos_a = polygon_label.find_polygon_label_position(a.polygon).trans(db.Point())
                pos_b = polygon_label.find_polygon_label_position(b.polygon).trans(db.Point())

                wire = airwire_shapes.insert(db.Path([pos_a, pos_b], 2))
                wire.set_property('net_name', net_name)

        # Insert the routes into the top-level cell.
        top_cell.insert(db.CellInstArray(airwire_cell.cell_index(), db.Trans(0, 0)))
