#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from ..klayout_helper import db
import logging

logger = logging.getLogger(__name__)


class RouterConfig:

    def __init__(self):
        self.routing_layers = dict()


class Router:

    def __init__(self, config: RouterConfig):
        assert isinstance(config, RouterConfig)
        self.config: RouterConfig = config

    def route(self,
              netlist: db.Circuit,
              layout: db.Layout,
              core_area: db.DSimplePolygon,
              top_cell: db.Cell,
              routing_terminals: db.Cell):
        assert isinstance(netlist, db.Circuit)
        assert isinstance(layout, db.Layout)
        assert isinstance(routing_terminals, db.Cell)
        return self.route_impl(netlist=netlist,
                               layout=layout,
                               core_area=core_area,
                               top_cell=top_cell,
                               routing_terminals=routing_terminals)

    def route_impl(self,
                   netlist: db.Circuit,
                   layout: db.Layout,
                   core_area: db.DSimplePolygon,
                   top_cell: db.Cell,
                   routing_terminals: db.Cell):
        raise NotImplemented()
