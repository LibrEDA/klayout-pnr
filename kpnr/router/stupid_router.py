#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import networkx as nx
from ..klayout_helper import db
from enum import Enum
from typing import Tuple, Dict, Any, Set, Optional, List, Union, Iterable
import numpy as np
from .router import *
from .graphrouter.pathfinder import PathFinderGraphRouter
from .graphrouter.signal_router import AStarRouter, DijkstraRouter
import logging
from collections import Counter, defaultdict

logger = logging.getLogger(__name__)


# class QuadTree:
#
#     def __init__(self, box: db.Box):
#         assert isinstance(box, db.Box)
#         self.box: db.Box = box
#         self.is_leaf: bool = True
#         self.leaf: Optional[db.Shape] = None
#         self.leaf_attributes: Optional[Any] = None
#         self.subtrees: Optional[List[QuadTree]] = None
#
#     def draw(self, output: db.Shapes) -> None:
#         output.insert(self.box)
#         if self.subtrees:
#             for st in self.subtrees:
#                 st.draw(output)
#
#     def insert(self, shape: db.Shape, attributes: Any = None) -> None:
#         if self.is_leaf and self.leaf is None:
#             self.leaf = shape
#             self.leaf_attributes = attributes
#         elif self.is_leaf and self.leaf is not None:
#             old_leaf = self.leaf
#             old_attributes = self.leaf_attributes
#             self.leaf = None
#             self.leaf_attributes = None
#             self.is_leaf = False
#
#             ll = self.box.p1
#
#             w = self.box.width()
#             h = self.box.height()
#             x0 = ll.x
#             y0 = ll.y
#
#             new_boxes = [
#                 db.Box(db.Point(x0 + w // 2, y0 + h // 2), db.Point(x0 + w, y0 + h)),
#                 db.Box(db.Point(x0, y0 + h // 2), db.Point(x0 + w // 2, y0 + h)),
#                 db.Box(db.Point(x0, y0), db.Point(x0 + w // 2, y0 + h // 2)),
#                 db.Box(db.Point(x0 + w // 2, y0), db.Point(x0 + w, y0 + h // 2)),
#             ]
#
#             self.subtrees = [QuadTree(b) for b in new_boxes]
#
#             self.insert(old_leaf, old_attributes)
#             self.insert(shape, attributes)
#         else:
#             shapes = db.Shapes()
#             shapes.insert(shape)
#             for st in self.subtrees:
#                 overlap = len(list(shapes.each_overlapping(st.box))) > 0
#                 if overlap:
#                     st.insert(shape, attributes)

class Tile:

    def __init__(self, index: Tuple[int, int], box: db.Box):
        """

        :param index: Index (x,y) position of the tile.
        :param box: The boundary of the tile.
        """
        self.index: Tuple[int, int] = index

        self.box: db.Box = box

        # Geometric shapes that are located in this tile.
        self.shapes: db.Shapes = db.Shapes()

        # Pairs of (terminal id, net name).
        self.terminals: Set[Tuple[int, Any]] = set()

        # Routing capacities to neighbouring tiles.
        self.capacity_north: int = 0
        self.capacity_east: int = 0
        self.capacity_west: int = 0
        self.capacity_south: int = 0
        self.capacity_bottom: int = 0
        self.capacity_top: int = 0
        self.through_capacity_x: int = 0
        self.through_capacity_y: int = 0

        # The smaller tiles that have been used to construct this tile.
        self.sub_tiles: List = []

        self.layer_index: int = 0

    def nets(self) -> Set[Any]:
        return {net for _, net in self.terminals}

    def compute_edge_capacities_from_geometry(self, wiring_pitch: int = 50) -> None:
        """
        Estimate the edge routing capacities based on the geometric shapes in this tile.
        """
        tile_size = self.box.width()
        tile_area = tile_size ** 2

        r_box = db.Region(self.box)
        r_shapes = db.Region(self.shapes)
        r_geometry = (r_box & r_shapes).merged()
        r_geometry.merged_semantics = True

        # Calculate via capacity by the ratio of the available free area and the via area.
        via_capacity = (tile_area - r_geometry.area()) / (wiring_pitch ** 2)
        self.capacity_top = via_capacity
        self.capacity_bottom = via_capacity

        # Compute edge wiring capacities.
        transf = [db.Trans.R0, db.Trans.R90, db.Trans.R180, db.Trans.R270]
        edge0 = db.Edge(db.Point(0, 0), db.Point(tile_size, 0))  # Edge in x-direction.
        edges = [edge0.transformed(tf) for tf in transf]
        # 'smear' the geometries in the direction of the rotated edge.
        rs_smeared = [(r_geometry.minkowsky_sum(edge) & r_box).merged() for edge in edges]

        # Calculate the capacities from the smeared geometries.
        edge_capacities = [(tile_size - smeared.area() // tile_size) // wiring_pitch for smeared in rs_smeared]

        [self.capacity_west, self.capacity_south, self.capacity_east, self.capacity_north] = edge_capacities

        # Compute through capacities.
        # (Part of the tile that has no obstructions through the whole tile in either x or y direction.)
        edge_x = db.Edge(db.Point(-tile_size, 0), db.Point(tile_size, 0))
        edge_y = db.Edge(db.Point(0, -tile_size), db.Point(0, tile_size))
        edges = [edge_x, edge_y]
        # 'smear' the geometry.
        rs_smeared = [(r_geometry.minkowsky_sum(edge) & r_box).merged() for edge in edges]
        through_capacities = [(tile_size - smeared.area() // tile_size) // wiring_pitch for smeared in rs_smeared]

        [self.through_capacity_x, self.through_capacity_y] = through_capacities

    def draw(self, shapes: db.Shapes) -> None:
        """
        Plot the box of the tile and set some interesting properties such that they can be inspected in KLayout.
        :param shapes: The output shape object.
        """
        instance = shapes.insert(self.box)

        nets = set(self.nets())
        ids = {id for id, net in self.terminals}
        instance.set_property('nets', nets)
        instance.set_property('terminal_ids', ids)

        instance.set_property('capacity_east', self.capacity_east)
        instance.set_property('capacity_west', self.capacity_west)
        instance.set_property('capacity_north', self.capacity_north)
        instance.set_property('capacity_south', self.capacity_south)
        instance.set_property('capacity_bottom', self.capacity_bottom)
        instance.set_property('capacity_top', self.capacity_top)
        instance.set_property('through_capacity_x', self.through_capacity_x)
        instance.set_property('through_capacity_y', self.through_capacity_y)

        instance.set_property('layer_index', self.layer_index)

    def __repr__(self):
        return "Tile {}".format(self.index)


class TiledShapes:
    """
    Represents the abstracted layout consisting of tiles.
    """

    def __init__(self, shapes: db.Shapes, box: db.Box, tile_size: int, layer_index: int):
        assert isinstance(box, db.Box) or isinstance(box, db.DBox)
        assert isinstance(shapes, db.Shapes)
        self.shapes: db.Shapes = shapes
        self.box: db.Box = box
        self.tiles: np.ndarray = None
        self.tile_size: int = tile_size
        self.layer_index: int = layer_index

    def load_from_shapes(self) -> None:
        """
        Create tiles and assign to them the shapes that are overlapping with the tile.
        :param tile_size: Side-length of the quadratic tiles.
        """
        tile_size = self.tile_size
        core_width = self.box.width()
        core_height = self.box.height()

        num_x = int((core_width + tile_size - 1) // tile_size)
        num_y = int((core_height + tile_size - 1) // tile_size)

        logger.debug("Creating {} * {} tiles of size {}.".format(num_x, num_y, tile_size))

        tile_boxes = np.ndarray((num_x, num_y), dtype=db.Box)

        for i in range(0, num_x):
            for j in range(0, num_y):
                box = db.Box(db.Point(i * tile_size, j * tile_size),
                             db.Point(i * tile_size + tile_size, j * tile_size + tile_size)
                             )
                tile_boxes[i, j] = (i, j), box

        shapes = self.shapes

        def create_tile(tile_box_with_index: Tuple[Any, db.Box]) -> Tile:
            index, tile_box = tile_box_with_index
            assert isinstance(tile_box, db.Box)

            tile = Tile(index, tile_box)
            tile.layer_index = self.layer_index

            for shape in shapes.each_overlapping(tile_box):
                assert isinstance(shape, db.Shape)

                if shape.is_polygon():
                    # TODO: what if it is a box, path, simple polygon?
                    polygon = shape.polygon
                    if polygon.touches(tile_box):
                        net = shape.property('net_name')
                        terminal_id = shape.property('terminal_id')
                        if net is not None or terminal_id is not None:
                            tile.terminals.add((terminal_id, net))
                        assert (net is None) == (
                                terminal_id is None), "`net_name` and `terminal_id` must both be either set or both not set."
                        tile.shapes.insert(shape)

            tile.compute_edge_capacities_from_geometry()

            return tile

        create_tile_vec = np.vectorize(create_tile)

        tiles = create_tile_vec(tile_boxes)

        # Make sure that each terminal appears only in one tile. TODO
        tiles_by_terminal = defaultdict(set)
        for tile in tiles.flat:
            assert isinstance(tile, Tile)
            for terminal in tile.terminals:
                tiles_by_terminal[terminal].add(tile)

        # Use only one tile per terminal.
        for tile in tiles.flat:
            tile.terminals.clear()

        for terminal, _tiles in tiles_by_terminal.items():
            for t in _tiles:
                t.terminals.add(terminal)
                break  # TODO

        self.tiles = tiles

    def draw_tiles(self, shapes: db.Shapes) -> None:
        """
        Draw all tile boxes.
        :param shapes:
        """
        for tile in self.tiles.flat:
            tile.draw(shapes)

    def coarsened(self, factor: int, ignore_nets: Set[Any]):
        """
        :param factor:
        :param ignore_nets: Set of nets that should not be propagated to the coarser level.
        :return:
        """
        assert isinstance(factor, int)
        assert factor > 0
        num_x, num_y = self.tiles.shape
        coarse_num_x = (num_x + factor - 1) // factor
        coarse_num_y = (num_y + factor - 1) // factor

        coarsened_tiles = np.ndarray((coarse_num_x, coarse_num_y), dtype=Tile)

        for x in range(0, coarse_num_x):
            for y in range(0, coarse_num_y):

                fine_tiles = self.tiles[x * factor:x * factor + factor, y * factor:y * factor + factor]
                bbox = sum((t.box for t in fine_tiles.flat), db.Box())
                tile = Tile((x, y), bbox)

                for t in fine_tiles.flat:
                    tile.terminals.update({(id, net) for id, net in t.terminals if net not in ignore_nets})
                    tile.shapes.insert(t.shapes)

                # Make the coarse tile remember its finer tiles.
                tile.sub_tiles = list(fine_tiles.flat)
                tile.layer_index = self.layer_index

                # TODO: Can be done more efficiently: Estimate edge capacities based on the edge capacities of the finer tiles.
                tile.compute_edge_capacities_from_geometry()

                coarsened_tiles[x, y] = tile

        coarsened = TiledShapes(self.shapes, self.box, self.tile_size * factor, layer_index=self.layer_index)
        coarsened.tiles = coarsened_tiles

        return coarsened

    def get_all_tiles(self) -> Iterable[Tile]:
        return self.tiles.flat


class TiledLayerStack:
    """
    Groups single layers together into a 3D layer stack.
    """

    def __init__(self, layers: List[TiledShapes]):
        self.layers: List[TiledShapes] = layers

        assert all((isinstance(l, TiledShapes) for l in layers))
        assert len(layers) > 0, "Cannot handle empty list of layers."

        # Assert that all layers have the same shape.
        shapes = [l.tiles.shape for l in layers]
        assert shapes[1:] == shapes[:-1], "Layers must all have the same shape."

        tile_sizes = [l.tile_size for l in layers]
        assert tile_sizes[1:] == tile_sizes[:-1], "Layers must all have the same tile size."
        self.tile_size = tile_sizes[0]

    def coarsened(self, factor: int, ignore_nets: Set[Any]):
        return TiledLayerStack(
            [l.coarsened(factor, ignore_nets) for l in self.layers]
        )

    def get_all_tiles(self) -> Set[Tile]:
        all_tiles = set()
        for l in self.layers:
            all_tiles.update(l.get_all_tiles())
        return all_tiles


def get_non_trivial_nets(layer_stack: TiledLayerStack) -> Set[Any]:
    """
    Get the nets which appear in more than one tile.
    :param layer_stack:
    :return: Set of non-trivial nets.
    """

    counted = Counter((net for t in layer_stack.get_all_tiles() for net in t.nets()))
    return {net for net, count in counted.items() if count > 1}


# class LambdaStack:
#
#     def __init__(self, layer_stack: TiledLayerStack):
#         self.stack: List[TiledLayerStack] = [layer_stack]
#         self.routing_regions: List[Dict[Any, Set[Tile]]] = [dict()]
#
#     def current_layer_stack(self) -> TiledLayerStack:
#         return self.stack[-1]
#
#     def current_routing_regions(self) -> Dict[Any, Set[Tile]]:
#         assert len(self.routing_regions) == len(self.stack)
#         return self.routing_regions[-1]
#
#     def coarsen(self, factor: int) -> None:
#         """
#         Go one level up in the hierarchy: Create a coarser representation of the layout.
#         :param factor: Coarsening factor.
#         :return:
#         """
#         ignore_nets = set(self.current_routing_regions().keys())
#         coarsened = self.current_layer_stack().coarsened(factor, ignore_nets)
#         self.stack.append(coarsened)
#         self.routing_regions.append(dict())
#
#     def refine(self) -> None:
#         coarse_regions = self.current_routing_regions()
#         # Propagate the coarse routing regions to the fine routing regions.
#         refined_regions = {
#             net: {t for coarse in coarse_region for t in coarse.sub_tiles}
#             for net, coarse_region in coarse_regions.items()
#         }
#         # Drop the coarsest level.
#         self.stack.pop()
#         self.routing_regions.pop()
#
#         # Update the routing regions of the finer level.
#         self.current_routing_regions().update(refined_regions)


class RoutingDirection(Enum):
    X = 0
    Y = 1


def create_routing_graph(stack: TiledLayerStack, first_layer_routing_direction: RoutingDirection) -> nx.Graph:
    """
    Convert a stack of layers into a 3D routing graph.
    :param stack:
    :param first_layer_routing_direction: Routing direction on lowest layer. Routing directions alternate from layer to layer.
    :return: The routing graph.
    """
    graph = nx.Graph()

    tiles_3d = np.array([l.tiles for l in stack.layers])

    tile_size = stack.tile_size
    edge_capacity = tile_size // 50
    edge_weight_xy = tile_size
    edge_weight_z = 1

    logger.debug("Create base routing graph: tile_size = {}".format(
        stack.tile_size
    ))

    dz, dx, dy = tiles_3d.shape
    assert dz == len(stack.layers)
    assert (dx, dy) == stack.layers[0].tiles.shape

    # Find out which layers are allowed to be routed in x direction.
    x_direction_indices = (np.arange(0, dz) + (1 if first_layer_routing_direction == RoutingDirection.X else 0)) % 2 == 1

    # Layers that are not allowed to be routed in x direction shall be routed in y direction.
    y_direction_indices = x_direction_indices ^ True

    # # # # TODO: remove this
    # For debugging allow routing in all directions on every layer.
    x_direction_indices = x_direction_indices > -1
    y_direction_indices = y_direction_indices > -1

    # Make connections in x direction.
    west_tiles = tiles_3d[x_direction_indices, :-1, :]
    east_tiles = tiles_3d[x_direction_indices, 1:, :]

    for s, t in zip(west_tiles.flat, east_tiles.flat):
        capacity = min(s.capacity_east, t.capacity_west)
        capacity = edge_capacity
        graph.add_edge(s, t, capacity=capacity, weight=edge_weight_xy)

    # Make connections in y direction.
    south_tiles = tiles_3d[y_direction_indices, :, :-1]
    north_tiles = tiles_3d[y_direction_indices, :, 1:]

    for s, t in zip(south_tiles.flat, north_tiles.flat):
        capacity = min(s.capacity_north, t.capacity_south)
        capacity = edge_capacity
        graph.add_edge(s, t, capacity=capacity, weight=edge_weight_xy)

    # Make connections in z direction.
    lower_tiles = tiles_3d[:-1, :, :]
    upper_tiles = tiles_3d[1:, :, :]

    for s, t in zip(lower_tiles.flat, upper_tiles.flat):
        capacity = min(s.capacity_top, t.capacity_bottom)
        graph.add_edge(s, t, capacity=capacity, weight=edge_weight_z)

    assert nx.is_connected(graph), "Routing graph should be connected."

    return graph


def route_tiles(layer_stack: TiledLayerStack,
                first_layer_routing_direction: RoutingDirection,
                coarsening_factor=8
                ) -> Dict[Any, nx.Graph]:
    # Find nets that would be swallowed by a single tile in the next coarsening step.
    print('route_tiles: tile_size = {}'.format(layer_stack.tile_size))
    # print('route_tiles(): {}'.format(layer_stack.layers[0].tiles.shape))
    num_of_nets = len({net for t in layer_stack.get_all_tiles() for net in t.nets()})
    if num_of_nets == 0:
        # Anchor of induction. Nothing to route.
        return dict()

    layer_stack_coarse = layer_stack.coarsened(coarsening_factor, set())
    counted_nets_coarse = Counter((net for t in layer_stack_coarse.get_all_tiles() for net in t.nets()))

    local_nets = {net for net, count in counted_nets_coarse.items() if count <= 1}
    # print('local_nets: ', local_nets)

    # Route the local nets.
    routing_graph = create_routing_graph(layer_stack, first_layer_routing_direction)
    tiles_by_net = defaultdict(list)
    # Find terminal tiles.
    for tile in routing_graph.nodes:
        for net in set(tile.nets()) & local_nets:
            tiles_by_net[net].append(tile)
    # print('terminals: {}'.format(tiles_by_net))

    # def euclidean_distance(s: Tile, t: Tile) -> int:
    #     """
    #     Distance heuristic for A-star router.
    #     """
    #     assert isinstance(s, Tile)
    #     assert isinstance(t, Tile)
    #     s_xy = s.box.center()
    #     t_xy = t.box.center()
    #     s_z = s.layer_index
    #     t_z = s.layer_index
    #
    #     dist1 = abs(t_z - s_z) + abs(t_xy.x - s_xy.x) + abs(t_xy.y - s_xy.y)
    #     return dist1
    #
    # signal_router = AStarRouter(euclidean_distance)
    signal_router = DijkstraRouter()
    router = PathFinderGraphRouter(signal_router, node_disjoint=False)
    local_routing_trees = router.route(graph=routing_graph, signals=tiles_by_net)
    local_routing_regions = {net_name: set(rt.nodes) for net_name, rt in local_routing_trees.items()}

    # Route the non-local nets.
    layer_stack_coarse = layer_stack.coarsened(coarsening_factor, ignore_nets=local_nets)
    coarse_nets = {n for t in layer_stack_coarse.get_all_tiles() for n in t.nets()}
    # print('coarse_nets', coarse_nets)

    assert len(coarse_nets & local_nets) == 0
    routing_trees_global = route_tiles(layer_stack_coarse, first_layer_routing_direction)
    routing_regions_global = {net_name: set(rt.nodes) for net_name, rt in routing_trees_global.items()}

    # Refine the coarse routing.
    routing_regions_refined = {
        net: {t for coarse in coarse_region for t in coarse.sub_tiles}
        for net, coarse_region in routing_regions_global.items()
    }

    routing_regions = dict()
    assert len(local_routing_regions.keys() & routing_regions_refined.keys()) == 0
    routing_regions.update(local_routing_regions)
    routing_regions.update(routing_regions_refined)

    routing_graph = create_routing_graph(layer_stack, first_layer_routing_direction)

    # Check that the routing regions are actually connected graphs.
    for net, nodes in routing_regions.items():
        g = routing_graph.subgraph(nodes).copy()
        assert nx.is_connected(g)

    tiles_by_net = defaultdict(list)
    # Find terminal tiles.
    for tile in routing_graph.nodes:
        for id, net in tile.terminals:
            tiles_by_net[net].append(tile)

    # num_nodes = len(routing_graph)
    # routing_region_relative_sizes = {
    #     net: len(region) / num_nodes for net, region in routing_regions.items()
    # }
    # print(routing_region_relative_sizes)
    #
    # print('refine signals: ', tiles_by_net.keys())
    result_routing_trees = router.route(graph=routing_graph, signals=tiles_by_net, allowed_nodes=routing_regions)

    # Sanity check: all routes should be trees.
    for tree in result_routing_trees.values():
        assert nx.is_tree(tree)

    return result_routing_trees


class StupidRouter(Router):
    """
    Simple stupid router implementation.
    """

    def route_impl(self,
                   netlist: db.Circuit,
                   layout: db.Layout,
                   core_area: db.DSimplePolygon,
                   top_cell: db.Cell,
                   routing_terminals: db.Cell):
        core_bbox = core_area.bbox()
        assert core_area.is_box(), "Core area must be a db.Box!"

        # TODO: Specify layers through API.
        routing_layers = [
            (21, 0),
            (23, 0),
            (25, 0),
            (27, 0),
            (29, 0),
            (31, 0),
        ]

        # Create flat view of the layout.
        flat_top: db.Cell = layout.create_cell('__stupid_router_flat_top')
        flat_top.copy_tree(top_cell)
        flat_top.flatten(True)

        # routing_shapes: List[db.Shapes] = [
        #     routing_terminals.shapes(layout.layer(db.LayerInfo(a, b)))
        #     for a, b in routing_layers
        # ]
        routing_shapes: List[db.Shapes] = []
        for a, b in routing_layers:
            labeled_terminals = routing_terminals.shapes(layout.layer(db.LayerInfo(a, b)))
            obstructions = flat_top.shapes(layout.layer(db.LayerInfo(a, b)))
            labeled_terminals.insert(obstructions)
            routing_shapes.append(labeled_terminals)

        tile_size = 200

        routing_tiles = [
            TiledShapes(shapes, core_bbox, tile_size=tile_size, layer_index=i)
            for i, shapes in enumerate(routing_shapes)
        ]
        for tiles in routing_tiles:
            tiles.load_from_shapes()

        stack_fine = TiledLayerStack(routing_tiles)

        # Route recursively.
        routing_trees = route_tiles(stack_fine, RoutingDirection.X)

        # Draw routing regions.
        signal_names = sorted(routing_trees.keys())
        for i, net_name in enumerate(signal_names):
            rt = routing_trees[net_name]
            l_debug_net = layout.layer(db.LayerInfo(130 + i, 0, net_name))
            debug_net_shapes = routing_terminals.shapes(l_debug_net)
            for node in rt.nodes:
                node.draw(debug_net_shapes)

        signal_names = sorted(routing_trees.keys())
        for i, net_name in enumerate(signal_names):
            rt = routing_trees[net_name]
            for e in rt.edges:
                a, b = e
                assert isinstance(a, Tile)
                assert isinstance(b, Tile)
                l1, l2 = a.layer_index, b.layer_index
                if l1 == l2:
                    c1 = a.box.center()
                    c2 = b.box.center()
                    path = db.Path([c1, c2], 50, 25, 25)
                    shape = routing_shapes[l1].insert(path)
                    shape.set_property('net_name', net_name)

        # for tiles, shapes in zip(routing_tiles, routing_shapes):
        #     tiles.draw_tiles(shapes)
