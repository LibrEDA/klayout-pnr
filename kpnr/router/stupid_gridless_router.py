#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import networkx as nx
from ..klayout_helper import db
import math
from enum import Enum
from typing import Tuple, Dict, Any, Set, Optional, List, Union, Iterable
import numpy as np
from .router import *
from .graphrouter.pathfinder import PathFinderGraphRouter
from .graphrouter.signal_router import AStarRouter, DijkstraRouter
import logging
from collections import Counter, defaultdict

logger = logging.getLogger(__name__)


class Dir(Enum):
    X = 0
    Y = 1

    def other(self):
        """ Get the other direction. """
        if self == Dir.X:
            return Dir.Y
        else:
            return Dir.X


class Point:

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def __repr__(self):
        return "Point({}, {})".format(self.x, self.y)

    def __eq__(self, other):
        if isinstance(other, Point):
            return self.__key() == other.__key()
        else:
            return False

    def __iter__(self):
        return iter((self.x, self.y))

    def __key(self):
        return self.x, self.y

    def __hash__(self):
        return hash(self.__key())

    def __abs__(self):
        return math.hypot(self.x, self.y)


class ExpansionPoint(Point):
    """
    An expansion point is essentially a `Point` but keeps an additional information: a delta offset.
    The delta offset tells whether this point is exactly on the coordinate or if it is slightly
    off by a tiny non-zero value.
    This is used to remember on which side of a line the expansion point lies.
    """

    def __init__(self, x: int, y: int, dx: int = 0, dy: int = 0):
        super().__init__(x, y)
        assert dx in (-1, 0, 1)
        assert dy in (-1, 0, 1)
        self.dx = dx
        self.dy = dy

    def to_point(self) -> Point:
        return Point(self.x, self.y)

    def __repr__(self):
        return "ExpansionPoint({}+{}, {}+{})".format(self.x, self.dx, self.y, self.dy)

    def __eq__(self, other):
        if isinstance(other, ExpansionPoint):
            return self.__key() == other.__key()
        elif isinstance(other, Point):
            return self.x == other.x and self.y == other.y
        else:
            return False

    def __iter__(self):
        return iter((self.x, self.y))

    def __key(self):
        return self.x, self.y, self.dx, self.dy

    def __hash__(self):
        return hash(self.__key())


class REdge:
    """
    Data-type for a rectilinear edge (line-segment) in the euclidean plane.
    A `REdge` is defined through a direction (vertical or horizontal),
    a starting point and a length. The starting point is the point with the lowest x or y coordinate on the edge.
    """

    def __init__(self, direction: Dir, p: Point, length: int):
        assert length >= 0
        assert isinstance(direction, Dir)
        assert isinstance(p, Point)
        self.direction: Dir = direction
        self.p: Point = p
        self.length: int = length
        self.attributes = None

    @staticmethod
    def from_klayout(edge: Union[db.Edge, db.DEdge]):
        """
        Convert a KLayout `Edge` or `DEdge` into a `REdge`. The edge needs to be horizontal or vertical.
        Otherwise an exception is thrown.
        :param edge:
        """
        p1 = edge.p1.x, edge.p1.y
        p2 = edge.p2.x, edge.p2.y

        return REdge.from_points(p1, p2)

    @staticmethod
    def from_points(p1: Tuple[int, int], p2: Tuple[int, int]):
        x1, y1 = p1
        x2, y2 = p2

        if x1 == x2:
            direction = Dir.Y
            ymin = min(y1, y2)
            ymax = max(y1, y2)

            return REdge(direction, Point(x1, ymin), ymax - ymin)
        elif y1 == y2:
            direction = Dir.X
            xmin = min(x1, x2)
            xmax = max(x1, x2)

            return REdge(direction, Point(xmin, y1), xmax - xmin)
        else:
            assert False, "Edge needs to be horizontal or vertical!"

    def is_horizontal(self) -> bool:
        return self.direction == Dir.X

    def is_vertical(self) -> bool:
        return self.direction == Dir.Y

    def start(self) -> Point:
        """
        Get start point of the edge.
        :return:
        """
        return self.p

    def end(self) -> Point:
        """
        Get end point of the edge.
        """
        x1, y1 = self.p
        if self.direction == Dir.X:
            x1 += self.length
        else:
            y1 += self.length
        return Point(x1, y1)

    def contains_point(self, p: Point) -> bool:
        """
        Check if the point lies on this edge. Inclusive endpoints.
        :param p:
        :return:
        """
        x1, y1 = self.p
        x2, y2 = p

        return (y1 == y2 and not (x2 > x1 + self.length or x1 > x2)) or \
               (x1 == x2 and not (y2 > y1 + self.length or y1 > y2))

    def intersects(self, other) -> bool:
        """
        Test if two edges intersect.
        """
        assert isinstance(other, REdge)

        return self.intersection(other) is not None

    def intersection(self, other):
        """
        Returns `None` if the edges don't intersect, a (x,y) tuple if they intersect in a point,
        or another `REdge` if the edges intersect in more than one point.
        :param other:
        :return:
        """
        assert isinstance(other, REdge)
        x1, y1 = self.p
        x2, y2 = other.p

        if self.direction == other.direction:
            if self.is_vertical() and x1 == x2:
                # Vertical
                max_start = max(y1, y2)
                min_end = min(y1 + self.length, y2 + other.length)
                l = min_end - max_start
                if l == 0:
                    return x1, max_start
                if l > 0:
                    return REdge(self.direction, Point(x1, max_start), l)
            elif self.is_horizontal() and y1 == y2:
                # Horizontal
                max_start = max(x1, x2)
                min_end = min(x1 + self.length, x2 + other.length)
                l = min_end - max_start
                if l == 0:
                    return max_start, y1
                if l > 0:
                    return REdge(self.direction, Point(max_start, y1), l)
            else:
                return None
        else:
            if self.is_vertical():
                vertical = self
                horizontal = other
            else:
                vertical = other
                horizontal = self

            line_intersection = Point(vertical.p.x, horizontal.p.y)

            if self.contains_point(line_intersection) and other.contains_point(line_intersection):
                return line_intersection
            else:
                return None

    def translated(self, v: Point):
        return REdge(self.direction, self.p + v, self.length)

    def rotated(self, angle: int):
        p1 = self.start()
        p2 = self.end()

        p = p2 - p1
        x, y = p

        angle = angle % 360

        if angle == 0:
            p = Point(x, y)
        elif angle == 90:
            p = Point(-y, x)
        elif angle == 180:
            p = Point(-x, -y)
        elif angle == 270:
            p = Point(y, -x)
        else:
            assert False, "Angle needs to be a integer multiple of 90."

        p2 = p + p1

        return REdge.from_points(p1, p2)

    def __repr__(self):
        d = self.direction
        return "REdge({}, {}, l={})".format(d, self.p, self.length)

    def __key(self):
        return self.direction, self.p, self.length

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other):
        if isinstance(other, REdge):
            return self.__key() == other.__key()
        else:
            return False


class REdges:

    def __init__(self):
        self.edges: Set[REdge] = set()

    def add_edge(self, edge: REdge):
        self.edges.add(edge)

    def add_edges(self, edges: Iterable[REdge]):
        self.edges.update(edges)

    def closest_ray_intersection(self, ray: REdge) -> Union[Point, REdge]:
        start_point = ray.start()
        intersections = {e.intersection(ray) for e in self.edges}
        intersection_points = {i for i in intersections if isinstance(i, Point) and i != start_point}
        overlaps = {i for i in intersections if isinstance(i, REdge)}

        if intersection_points:
            closest_intersection_point = min(intersection_points, key=lambda i: abs(start_point - i))
        else:
            return None


def test_redge():
    """
    Sanity tests for `REdge`.
    :return:
    """

    # Intersection in starting point.
    e1 = REdge(Dir.X, Point(0, 0), 10)
    e2 = REdge(Dir.Y, Point(0, 0), 10)
    assert e1.intersection(e2) == Point(0, 0)

    # Intersection in a point.
    e1 = REdge(Dir.X, Point(0, 2), 10)
    e2 = REdge(Dir.Y, Point(1, 0), 10)
    assert e1.intersection(e2) == Point(1, 2)

    # No intersection.
    e1 = REdge(Dir.X, Point(1, 0), 10)
    e2 = REdge(Dir.Y, Point(0, 0), 10)
    assert e1.intersection(e2) is None

    # Full overlap.
    e1 = REdge(Dir.X, Point(0, 0), 10)
    e2 = REdge(Dir.X, Point(0, 0), 10)
    assert e1.intersection(e2) == REdge(Dir.X, Point(0, 0), 10)

    # Partial overlap
    e1 = REdge(Dir.X, Point(1, 0), 10)
    e2 = REdge(Dir.X, Point(0, 0), 9)
    assert e1.intersection(e2) == REdge(Dir.X, Point(1, 0), 8)

    e1 = REdge(Dir.X, Point(1, 0), 10)
    e2 = REdge(Dir.X, Point(0, 0), 9)
    assert e1.intersection(e2) == REdge(Dir.X, Point(1, 0), 8)


def test_beam():
    import matplotlib.pyplot as plt

    ray_lenght = 200  # Rays are implemented as edges of finite length.

    # Edges of obstructions.
    edges = [
        REdge(Dir.X, Point(10, 10), 40),
        REdge(Dir.X, Point(30, 30), 40),
        # REdge(Dir.Y, Point(80, 10), 60),
        # REdge(Dir.Y, Point(0, 0), 60),
    ]

    def plotEdge(edge: REdge, *args, **kw):
        x1, y1 = edge.start()
        x2, y2 = edge.end()
        plt.plot([x1, x2], [y1, y2], *args, **kw)

    # Plot obstructions.
    for e in edges:
        plotEdge(e, 'b')

    def new_ray(p: ExpansionPoint, angle: int) -> REdge:
        ray0 = REdge(Dir.X, p, ray_lenght)
        return ray0.rotated(angle)

    # Define root of the expansion graph.
    front = {(ExpansionPoint(20, 20), None)}  # Store (point, incoming angle) pairs.
    visited = set()

    # Do expansion.
    while front:
        expansion_point, incoming_angle = front.pop()

        if expansion_point in visited:
            continue

        plt.plot([expansion_point.x], [expansion_point.y], 'rx')
        print(expansion_point)

        visited.add(expansion_point)

        ray0 = REdge(Dir.X, expansion_point, ray_lenght)

        # Expand in all four directions.
        for angle in range(0, 360, 90):
            if angle != incoming_angle:
                # Create ray into the current direction with origin at the expansion point.
                ray = ray0.rotated(angle)

                intersections = {e.intersection(ray) for e in edges}
                intersection_points = {i for i in intersections if isinstance(i, Point) and i != expansion_point}
                overlaps = {i for i in intersections if isinstance(i, REdge)}

                if intersection_points:
                    closest_intersection_point = min(intersection_points, key=lambda i: abs(expansion_point - i))
                    diff = expansion_point - closest_intersection_point
                    if diff.x == 0:
                        dx = expansion_point.dx
                    else:
                        dx = math.copysign(1, diff.x)
                    if diff.y == 0:
                        dy = expansion_point.dy
                    else:
                        dy = math.copysign(1, diff.y)
                    new_expansion_point = ExpansionPoint(closest_intersection_point.x, closest_intersection_point.y,
                                                         dx, dy)

                    ray = REdge.from_points(expansion_point, closest_intersection_point)

                    if ray.length > 0:
                        front.add((new_expansion_point, angle))

                    plotEdge(ray, 'g-', alpha=0.2)
                else:
                    plotEdge(ray, 'r-', alpha=0.2)

                if overlaps:
                    for o in overlaps:
                        #
                        furthest_intersection_point = max([o.start(), o.end()], key=lambda i: abs(expansion_point - i))
                        # Only add the end of the overlap if it comes before any intersection.

                        diff = furthest_intersection_point - expansion_point

                        if abs(diff) < ray.length:

                            if diff.x == 0:
                                dx = expansion_point.dx
                            else:
                                dx = math.copysign(1, diff.x)
                            if diff.y == 0:
                                dy = expansion_point.dy
                            else:
                                dy = math.copysign(1, diff.y)

                            new_expansion_point = ExpansionPoint(furthest_intersection_point.x,
                                                                 furthest_intersection_point.y,
                                                                 dx, dy)

                            front.add((new_expansion_point, angle))

    # plt.show()


class StupidGridlessRouter(Router):
    """
    Simple stupid gridless router implementation.
    """

    def route_impl(self,
                   netlist: db.Circuit,
                   layout: db.Layout,
                   core_area: db.DSimplePolygon,
                   top_cell: db.Cell,
                   routing_terminals: db.Cell):
        core_bbox = core_area.bbox()
        assert core_area.is_box(), "Core area must be a db.Box!"

        # TODO: Specify layers through API.
        routing_layers = [
            (21, 0),
            (23, 0),
            (25, 0),
            (27, 0),
            (29, 0),
            (31, 0),
        ]

        # Create flat view of the layout.
        flat_top: db.Cell = layout.create_cell('__stupid_router_flat_top')
        flat_top.copy_tree(top_cell)
        flat_top.flatten(True)

        # routing_shapes: List[db.Shapes] = [
        #     routing_terminals.shapes(layout.layer(db.LayerInfo(a, b)))
        #     for a, b in routing_layers
        # ]
        routing_shapes: List[db.Shapes] = []
        for a, b in routing_layers:
            labeled_terminals = routing_terminals.shapes(layout.layer(db.LayerInfo(a, b)))
            obstructions = flat_top.shapes(layout.layer(db.LayerInfo(a, b)))
            labeled_terminals.insert(obstructions)
            routing_shapes.append(labeled_terminals)

        # for tiles, shapes in zip(routing_tiles, routing_shapes):
        #     tiles.draw_tiles(shapes)
