#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Detect pins in the layout based on text labels.
"""

from typing import Dict
from ..klayout_helper import db
import collections
import logging

logger = logging.getLogger(__name__)


def find_pins_from_text_labels(cell: db.Cell, pin_shape_layer_id: int, label_layer_id: int) -> Dict[str, db.Region]:
    """
    Find pin shapes based on a layer containing all polygon shapes and a layer containing text labels.
    The text labels mark the shapes that belong to a pin. For each text label all this shapes are grouped together
    and returned in a dictionary which maps pin names to a `db.Region` containing the pin shapes. (Dict[pin name, db.Region]).
    :param cell: The cell of which pins should be found.
    :param pin_shape_layer_id: The layer which contains the pin shapes.
    :param label_layer_id: The layer which contains the text labels of the pins.
    :return: Dict[pin name, db.Region]
    """
    # Fetch the shapes of the layers.
    shapes_pin = cell.shapes(pin_shape_layer_id)
    shapes_label = cell.shapes(label_layer_id)

    # Group the locations of the labels by pin name.
    labels_locations_by_pin_name = collections.defaultdict(set)
    for l in shapes_label.each():
        assert isinstance(l, db.Shape)
        if l.is_text():
            text = l.text
            location = db.Point(text.x, text.y)
            # logger.debug("Pin label of cell {}: {}".format(cell.name, l))
            labels_locations_by_pin_name[text.string].add(location)
        else:
            logger.warning("Label layer contains non-text shape: {} ({})".format(l, type(l)))

    # For each pin name find all shapes that interact with the label locations.
    pin_shapes = collections.defaultdict(set)
    for label_name, label_locations in labels_locations_by_pin_name.items():
        # Find shapes that are interacting with the labels.
        # First create tiny boxes at the label locations. (KLayout cannot find interactions of points and polygons.)
        balls = [db.Box(db.Point(l.x, l.y), db.Point(l.x + 1, l.y + 1)) for l in label_locations]
        interacting_shapes = db.Region(shapes_pin).interacting(db.Region(balls)).merged()

        # Store the result.
        pin_shapes[label_name] = interacting_shapes

    return pin_shapes
