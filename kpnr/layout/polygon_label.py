#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Find label positions in a polygon.
"""

from ..klayout_helper import db


def find_polygon_label_position(polygon: db.Polygon, label_text: str = '') -> db.Trans:
    """
    Find good position for a label inside a polygon.
    :param polygon: The polygon to put the label inside.
    :return: Return a `db.Trans` that describes the location and rotation of the label.
    """

    assert isinstance(polygon, db.Polygon)
    assert not polygon.is_empty(), "Polygon must not be empty."
    bbox = polygon.bbox()
    bbox_center = bbox.center()

    trapezoids = polygon.decompose_convex()

    # Find all center points of the convex parts.
    centers = [p.bbox().center() for p in trapezoids]

    # Find the trapezoid which has its center closest to the bbox_center.
    closest = min(centers, key=lambda c: bbox_center.distance(c))

    return db.Trans(closest.x, closest.y)
