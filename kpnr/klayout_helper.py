#
# Copyright (c) 2020-2021 Thomas Kramer.
#
# This file is part of klayout-pnr 
# (see https://codeberg.org/libreda/klayout-pnr).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Helper functions for KLayout macros.
"""

import logging

logger = logging.getLogger(__name__)

try:
    print("Try to import pya.db.")
    logger.debug("Try to import pya.db.")
    import pya as db
    print("Imported pya.db.")
    logger.debug("Imported pya.db (running within KLayout).")
except ImportError as e:
    print("Try to import klayout.db.")
    logger.debug("Try to import klayout.db.")
    import klayout.db as db
    print("Imported klayout.db.")
    logger.debug("Imported klayout.db.")
