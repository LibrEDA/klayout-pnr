# Simple ASIC Place-and-Route Framework for Python & KLayout

`klayout-pnr` is a simple Python framework for place & route of digital circuits.
It is based on KLayout and hence integrates also into the KLayout user interface.

## Install (Debian 10)

Create a virtual Python environment:
```bash
python3 -m venv myenv
source ./myenv/bin/activate
```

Clone the code:
```bash
git clone [this repo]
cd [this repo]
```

Install the package:
```bash
python3 setup.py develop
```

## Integrate into KLayout

Since this project is still in an early stage, there's no registered KLayout plugin package yet.

Therefore it is recommended to install the KLayout plugin by manually creating a symlink to the `klayout-macros` directory of this repo:

```sh
# Make the macros accessible from KLayout.
ln -s $PWD/klayout-macros ~/.klayout/pymacros/kpnr
```

Now the macros should be available in KLayout under `Macro Development`, `Python`-tab, `Local`, `kpnr`.

The macros are numbered in the order of execution. To execute a macro, open it and press `Shift+F5`.

# KLayout Macros

A set of macros illustrate how the framework can be used from within KLayout. The macros are numbered by
the order of execution. Each of them does some part of the design flow.

* init: Load Python libraries, setup logging.
* init_tech: Load design libraries and technology related data such as design rules.
* init_design: Load the netlist that should be implemented.
* floorplan: Define the core area, pin locations and put all cells into the layout (all at the same location yet).
* place: Call the global placement algorithm to find rough positions for all cells.
* legalize: Refine the placement such that cells do not overlap and are properly aligned to the rows.
* power_routing: Insert power stripes that route VDD and GND to all the cells.
* signal_routing: Create the wires such that the cells in the layout are connected exactly the same as in the netlist. *This is not implemented yet, for connections are only visualized with diagonal wires.*
